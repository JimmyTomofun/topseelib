//
//  Funclib.h
//  Funclib
//
//  Created by 廖海洲 on 12-11-12.
//  Copyright (c) 2012年 Topsee. All rights reserved.
//

#ifndef Funclib_Funclib_h
#define Funclib_Funclib_h

#define TPS_MSG_BASE 0x2000

/************************************************************************************************************************
*                                        xml 摄像机设备数据格式定义说明                                                          *
*************************************************************************************************************************／
<?xml version="1.0" encoding="utf-8"?>
<DeviceList>
    <Device>
        <DevId>设备id</DevId>
        <DevName>设备名称</DevName>
        <DevGroupName>设备所属组名称</DevGroupName>
        <OnLine>设备是否在线,0:offline,1:heartbeat lost,2:active online</OnLine>
        <WithPTZ>是否支持云台控制,0:false,1:true</WithPTZ>
        <DoubleStream>是否支持双码流,0:false,1:true</DoubleStream>
        <RecStatus>是否处于录像状态,0:false,1:true</RecStatus>
        <VisitStreamOption>允许访问的码流,0:不禁止，1:禁止访问第0路，2:禁止访问第1路，3：禁止访问第0路，第1路。</VisitStreamOption>
        <DevType>设备类型，100：表示IPC，200：表示NVR</DevType>
    </Device>
    <Device>
        .
        .
        .
    <Device>
    .
    .
    .
</DeviceList>
************************************************************************************************************************/





/************************************************************************************************************************
 *                                        xml 报警器设备列表以及告警数据格式定义说明                                           *
 *************************************************************************************************************************／
 
 //报警器设备列表
 <DevId>摄像机id</DevId>
 <ALERTOR_LIST>
 <ALERTOR ADDR_CODE=4 ALIAS="烟感_门口" ENABLE=1 />
 <ALERTOR ADDR_CODE=5 ENABLE=1 />
           .
           .
           .
 </ALERTOR_LIST>
 
 
 附注：DevId:摄像机ID
      ADDR_CODE : 告警器地址码
      ALIAS ：告警器别名，没有或者为空（用户未设置别名）
      ENABLE：布撤防标志， 1为布防， 0为撤防
 
 //报警器产生的告警消息
 <ALARM ID=5 DEV_TYPE=2 DEV_ADDR=4 EVENT_TYPE=1 DEV_ALIAS="门磁" DEV_SECURITY=1 />
 
 附注：ID ：告警ID唯一标识此告警
      DEV_TYPE:告警器设备类型， 1为门磁，2为烟感 3为燃气
      EVENT_TYPE:告警事件类型， 1为普通告警，2低压告警，3心跳告警
      DEV_ALIAS:告警器别名，如果没设置则没有
      DEV_SECURITY:布防状态
 
 
 
 ************************************************************************************************************************/





/************************************************************************************************************************
 *                                              消息与结构定义说明                                                         *
 ************************************************************************************************************************/
typedef struct
{
	unsigned int userManagerRight;//用户管理
	unsigned int puManagerRight;//设备管理
	unsigned int videoSuvRight;//视频浏览
	unsigned int ptzSetRight;//云镜控制
	unsigned int alarmManagerRight;//告警管理
	unsigned int videoPalybackRight;//录像回放
	unsigned int conManagerRight;//内容管理
	unsigned int conDeleteRight;//内容删除
	unsigned int tvwallManagerRight;//电视墙
	unsigned int audioTwoWayRight;//语音对讲
	unsigned int audioBrardCastRight;//语音广播
	unsigned short nUserPriority;//用户优先级
}UserRight;

#define VS_DEV_ID_LEN 32
#define MAX_VIDEO_AUDIO_CODEC_LEN 32
//视频解码参数
typedef struct
{
	unsigned int	    stream_index;
	char	video_encoder[MAX_VIDEO_AUDIO_CODEC_LEN];
	unsigned int 	width;
	unsigned int 	height;
	unsigned int 	framerate;
	unsigned int 	intraframerate;  //I frame interval
	unsigned int 	bitrate;
	char	config[256]; //提交给解码器的第一个I帧前面必须加上config的数据
	int	    config_len; //MPEG4 18字节VOL，H264 114字节
}TPS_VIDEO_PARAM;

//音频解码参数
typedef struct
{
	unsigned int     stream_index;
	char             audio_encoder[MAX_VIDEO_AUDIO_CODEC_LEN];
	unsigned int     samplerate;
	unsigned int     samplebitswitdh; //8 or 16
	unsigned int     channels; //0: mono, 1: stero
	unsigned int     bitrate;
	unsigned int     framerate;
}TPS_AUDIO_PARAM;
 

#define  MP4FILE_HANDLE     void *



//回放与直连访问用到的音视频参数结构体
//  tcp连接媒体服务器后需要发送的通知消息头结构定义。使用说明：客户端tcp连接上媒体服务器后需要向服务器发送一个通知消息，通知消息头的填写参考下面结构体字段注释 // 


typedef struct __NetSDK_VIDEO_PARAM
{
	char codec[256];
	int width;
	int height;
	int colorbits;
	int framerate;
	int bitrate;
	char vol_data[256];
	int vol_length;
}NetSDK_VIDEO_PARAM;

typedef struct __NetSDK_AUDIO_PARAM
{
	char codec[256];
	int samplerate;
	int bitspersample;
	int channels;
	int framerate;
	int bitrate;
}NetSDK_AUDIO_PARAM;
//


//tcp连接媒体服务器后需要发送的通知消息头结构定义。使用说明：客户端tcp连接上媒体服务器后需要向服务器发送一个通知消息，通知消息头的填写参考下面结构体字段注释
typedef struct
{
	unsigned int magic;		//填0x69707673。字段说明：此字段为天视通识别字段。
	unsigned int nSvrInst;	//此id通过TPS_AddWachtRsp消息返回的。字段说明：VSS session ID，根据此ID 媒体服务器可找到Client的所有信息。
	unsigned char nDataSrc;	//填0: 表示是客户端连接。
	unsigned char nPacketType;	//填0: 表示是通知消息
	unsigned short nDataLength;//填0：通知消息不需要后续数据包
}TPS_VsTcpPacketHeader;

//媒体数据包头结构定义
typedef struct
{
    unsigned int frame_timestamp; //此帧对应的时间戳，用于音视频同步，一帧中的不同包时间戳相同
    unsigned int keyframe_timestamp; //如果是非I帧，记录其前一I帧的timestamp，如果解码器没有收到前面那个I帧，所有非I帧丢掉丢包不解码
    unsigned short pack_seq; //包序号0-65535，到最大后从0开始
    unsigned short payload_size; //此包中包含有效数据的长度
    unsigned char pack_type; //0x01第一包，0x10最后一包, 0x11第一包也是最后一包，0x00中间包
    unsigned char frame_type; //帧类型1：I帧，0：非I帧
    unsigned char stream_type; //0: video, 1: audio，2：发送报告，3：接收报告，4：打洞包
    unsigned char stream_index;
    unsigned int frame_index;
}TPS_STREAM_PACKET_HEADER;

typedef struct
{
    int     bIsKey;
    double  timestamp;
    int     nChannelId;//nvr channel id
}TPS_EXT_DATA;

//视频请求后返回的消息通知，nResult＝0表示视频请求成功，客户端根据nVssSvrIP（媒体服务器地址），nTransPro（媒体传输协议）以及对应的端口进行收流。
//如果是tcp媒体传输协议，客户端主动与nMediaSendPort（媒体服务器发流端口）进行主动连接收流，需要注意的是tcp连接后需要向服务器发送TPS_VsTcpPacketHeader通知消息。
//如果是udp媒体传输协议，客户端直接绑定nMediaRecvPort端口进行收流。
//还需要判断是单播还是组播，如果是组播需要加入组播地址端口进行收流。
//客户端收到媒体流后需要根据数据包头TPS_STREAM_PACKET_HEADER信息进行组包，组包后自己解码显示。
typedef struct
{
    char szDevId[VS_DEV_ID_LEN];//设备id
    int  nResult;//请求结果，0表示成功，非零失败
    int  nTransPro;//媒体传输协议，0:udp,1:tcp
    int  nFSM;//1:表示单播，2:表示组播
    int  nMulIp;//组播地址
    int  nMulPort;//组播端口
    int  nMediaType;//媒体类型，0x0001:视频， 0x0100:音频， 0x0101：音视频
    int  nMediaSendPort;//udp：表示服务器发流端口，需要客户端向本端口打洞；tcp：表示tcp服务器端口，由客户端主动连接。
    int  nMediaRecvPort;//udp本地收流端口
    int  nVssSvrIP;//媒体服务器地址
    int  nSvrInst;//VM@该路视频请求服务器保存的session id，tcp连接发送通知消息的时候需要带上。P2P@nvr channelid
    TPS_VIDEO_PARAM videoParam;//视频解码参数
    TPS_AUDIO_PARAM audioParam;//音频解码参数
}TPS_AddWachtRsp;

#define DEV_FILE_LEN  1024
typedef struct
{
    char szDevId[VS_DEV_ID_LEN];//设备ID
    char szReplayFile[DEV_FILE_LEN];//请求回放文件名称
    int nVideoSecs;//回放文件时间长度，单位秒
    int  nResult;//请求结果，0表示成功，非0失败
    int  nActionType;//请求动作，如：播放，暂停，快进，快退等，参考REPLAY_IPC_ACTION枚举值
    short  bHaveVideo;//是否带视频参数
	short  bHaveAudio;//是否带音频参数
	NetSDK_VIDEO_PARAM videoParam;//视频参数
	NetSDK_AUDIO_PARAM audioParam;//音频参数
}TPS_ReplayDevFileRsp;


typedef struct
{
    char szDevId[VS_DEV_ID_LEN];//设备id
    int  nResult;//请求结果，0表示成功，非零失败
    int  nTransPro;//媒体传输协议，0:udp,1:tcp
    int  nVssSvrIP;//媒体服务器地址
    int  nMediaSendPort;//udp：表示服务器发流端口，需要客户端向本端口打洞；tcp：表示tcp服务器端口，由客户端主动连接。
    int  nMediaRecvPort;//udp本地收流端口
    int  nSvrInst;//该路视频请求服务器保存的session id，tcp连接发送通知消息的时候需要带上。
    TPS_AUDIO_PARAM audioParam;//音频解码参数
}TPS_TALKRsp;

//消息类型定义
enum TPS_MSG_TYPE
{
    TPS_MSG_NOTIFY_LOGIN_OK = TPS_MSG_BASE+1,//登录成功,返回用户权限 //8193
    TPS_MSG_NOTIFY_LOGIN_FAILED,//登录失败,返回设备ID
    TPS_MSG_NOTIFY_DEV_DATA,//登录成功后lib库主动返回xml格式设备数据,具体格式请参见上面说明
    TPS_MSG_RSP_ADDWATCH,//视频请求响应，返回TPS_AddWachtRsp //8196
    TPS_MSG_RSP_PTZREQ,//云台请求响应，返回是否请求成功
    TPS_MSG_RSP_PTZACTION,//云台控制响应，返回云台控制是否成功
    TPS_MSG_RSP_TALK,//对讲请求响应，返回TPS_TALKRsp
    TPS_MSG_RSP_TALK_CLOSE,//对讲停止响应， 返回TPS_NotifyInfo
    TPS_MSG_NOTIFY_CONFIG_CHANGED, //配置更新通知
    TPS_MSG_ALARM,//报警消息，返回TPS_AlarmInfo
    TPS_MSG_EVENT,//事件消息, 返回TPS_EventInfo
    TPS_MSG_P2P_INIT_FAILED,//P2P初始化失败,返回TPS_NotifyInfo
    TPS_MSG_P2P_SELF_ID,//P2P客户实列ID，返回TPS_NotifyInfo
    TPS_MSG_P2P_CONNECT_OK,//P2P连接成功，返回对应的设备ID
    TPS_MSG_REC_STOP,//record stop message, return TPS_NotifyInfo
    TPS_MSG_RSP_START_ALERTOR_BIND,//设备开始对码响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_STOP_ALERTOR_BIND,//设备停止对码响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_GET_ALERTOR_LIST_FAILED,//获取设备对码成功的报警器列表失败，返回TPS_NotifyInfo
    TPS_MSG_RSP_GET_ALERTOR_LIST_OK,//获取设备对码成功的报警器列表成功，返回xml数据具体格式请参见上面说明
    TPS_MSG_RSP_DEL_ALERTOR_BIND,//删除已对码成功的报警器响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_SECURITY_SET,//布撤防响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_ALERTOR_ALIAS_SET,//报警器别名设置响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_ALERTOR_PTZ_SET,//针对报警器进行云台预置点设置响应，返回TPS_NotifyInfo
    TPS_MSG_NOTIFY_ALERTOR_ALM,//报警器上报的告警消息，返回xml数据，具体格式参见上面说明
    TPS_MSG_RSP_GET_ALM_PIC,//获取报警图片响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_SECURITY_GET_FAILED,//获取设备的布撤防状态失败，返回TPS_NotifyInfo
    TPS_MSG_RSP_SECURITY_GET_OK,//获取设备的布撤防状态成功，返回xml数据，具体格式参见上面说明
    TPS_MSG_RSP_ALARM_CONFIRM,//确认报警器告警响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_SEARCH_ALARM,//设备告警记录查询结果，返回xml数据
    TPS_MSG_RSP_GET_FRIEND_LIST,//获取好友列表响应
    TPS_MSG_RSP_GET_OFFLINE_MSG,//获取好友离线消息
    TPS_MSG_RSP_REPLAY_DEV_FILE,//前端录像回放响应，返回TPS_ReplayDevFileRsp结构
    TPS_MSG_RSP_GET_DEV_PIC,//获取前端设备图片响应，返回TPS_NotifyInfo
    TPS_MSG_RSP_SEARCH_OSS_OBJECTLIST,//获取云存储object列表，返回xml
    TPS_MSG_RSP_OSS_REPLAY_PARAM,
    TPS_MSG_RSP_OSS_REPLAY_FINISH,//OSS回放结束
    TPS_MSG_BEGIN_DOWNLOAD_OSS_OBJECT,//开始下载OSS文件
    TPS_MSG_DOWNLOAD_OSS_OBJECT_SIZE,//下载大小通知
    TPS_MSG_END_DOWNLOAD_OSS_OBJECT,//下载结束
    TPS_MSG_DOWNLOAD_OSS_OBJECT_FAILED,//下载失败
    TPS_MSG_OSS_PLAY_BEGIN_CACHE,//开始播放缓冲
    TPS_MSG_OSS_PLAY_END_CACHE,//结束播放缓冲
    TPS_MSG_P2P_NETTYPE,//IPC device conncet net type
    TPS_MSG_P2P_OFFLINE,//device offline
    TPS_MSG_P2P_NVR_OFFLINE,//NVR device offline
    TPS_MSG_P2P_NVR_CH_OFFLINE,//NVR channel offline
    TPS_MSG_P2P_NVR_CH_ONLINE,//NVR channel online
    TPS_MSG_RSP_UPDATE_FIRMWARE,//request update firmware response, return TPS_NotifyInfo
    TPS_MSG_RSP_TOSS_FOOD,//request toss food response, return TPS_NotifyInfo
    TPS_MSG_RSP_UPLOAD_FILE,//request upload file response,return TPS_NotifyInfo
    TPS_MSG_RSP_UPLOAD_PROGRESS,//upload file progress, return xml info
    TPS_MSG_RSP_UPLOAD_FAILED,//upload file failed, return xml info
    TPS_MSG_RSP_UPLOAD_OK,//upload file success, return xml info
    TPS_MSG_RSP_BARKING_VOLUME,//set barking volume response, return TPS_NotifyInfo
    TPS_MSG_RSP_S3_REC,//request s3 record response, return TPS_NotifyInfo
    TPS_MSG_NOTIFY_AUTH_FAILED,//device authrication failed, return TPS_NotifyInfo
    TPS_MSG_P2P_NVR_CONNECT_REFUSE,////nvr4.0 connect failed, return TPS_NotifyInfo
    TPS_MSG_P2P_NVR_TST_RPT,//nvr4.0 report info, return TPS_NotifyInfo
    TPS_MSG_RSP_SEARCH_NVR_REC,//response search nvr record result,return TPS_NotifyInfo.szInfo contain char "ABCCC...."
    TPS_MSG_RSP_NVR_REPLAY,//nvr replay by time response, return TPS_ReplayDevFileRsp
    TPS_MSG_NOTIFY_DISP_INFO,//notify display info, return TPS_NotifyInfo
    TPS_MSG_RSP_SET_SPEAKER_VOLUME,//response set speaker volume, return TPS_NotifyInfo
    TPS_MSG_RSP_GET_SPEAKER_VOLUME,//response get speaker volume, return TPS_NotifyInfo
    TPS_MSG_RSP_GET_BARKING_VOLUME,//get barking volume response, return TPS_NotifyInfo
    TPS_MSG_NOTIFY_P2P_TCP_TIMEOUT,//response tcp timeout, return TPS_NotifyInfo
    TPS_MSG_RSP_UPDATE_FW_INFO,//response get update firmware info, return xml
    TPS_MSG_RSP_GET_SERVICE_MSG_LIST,//response get service message info, return xml
    TPS_P2P_NETWORK_QS_INFO,    //network QS info
    TPS_P2P_NOTIFY_SVR_LOGIN_OK,    //p2p server login ok
    TPS_MSG_RSP_GET_SERVICE_MSG_COMMENT_LIST,// response service comment info, return xml
    TPS_MSG_NOTIFY_BEGIN_RECORD,
    TPS_MSG_NOTIFY_END_RECORD,
    TPS_MSG_NOTIFY_P2P_THREAD_EXIT,
};

typedef struct
{
	char szDevId[VS_DEV_ID_LEN];
	int  nResult;
	char szInfo[1444];
}TPS_NotifyInfo;

//告警类型定义
enum TPS_ALARM_TYPE
{
    TPS_ALARM_NONE = 0,
    TPS_ALARM_FIRE,//火警
    TPS_ALARM_SMOKE,//烟警
    TPS_ALARM_INFRARED,//红外报警
    TPS_ALARM_GAS,//气体报警
    TPS_ALARM_TEMPERATURE,//温度报警
    TPS_ALARM_GATING,//门控报警
    TPS_ALARM_MANUAL,//手动报警
    TPS_ALARM_FRAME_LOST,//视频丢失报警
    TPS_ALARM_MOTION,//移动侦测报警
    TPS_ALARM_MASKED,//视频遮挡报警
    
    TPS_ALARM_LINKDOWN=21,//设备掉线
    TPS_ALARM_LINKUP,//设备上线
    TPS_ALARM_USB_PLUG,//USB插上
    TPS_ALARM_USB_UNPLUG,//USB拔掉
    TPS_ALARM_SD0_PLUG,//SD1插上
    TPS_ALARM_SD0_UNPLUG,//SD1拔掉
    TPS_ALARM_SD1_PLUG,//SD2插上
    TPS_ALARM_SD1_UNPLUG,//SD2拔掉
    TPS_ALARM_USB_FREESPACE_LOW,//USB空间不足
    TPS_ALARM_SD0_FREESPACE_LOW,//SD1卡空间不足
    TPS_ALARM_SD1_FREESPACE_LOW,//SD2卡空间不足
    TPS_ALARM_VIDEO_LOST,//视频丢失
    TPS_ALARM_VIDEO_COVERD,//视频遮挡
    TPS_ALARM_MOTION_DETECT,//移动侦测
    TPS_ALARM_GPIO3_HIGH2LOW,//告警输入高变低
    TPS_ALARM_GPIO3_LOW2HIGH,//告警输入低变高
    TPS_ALARM_STORAGE_FREESPACE_LOW,//存储空间不足
    TPS_ALARM_RECORD_START,//录像开始
    TPS_ALARM_RECORD_FINISHED,//录像结束
    TPS_ALARM_RECORD_FAILED,//录像失败
    TPS_ALARM_GPS_INFO,//GPS信息
    TPS_ALARM_EMERGENCY_CALL,//紧急呼叫
    TPS_ALARM_JPEG_CAPTURED,//控制前端抓图报警
    TPS_ALARM_RS485_DATA, //前端串口数据报警
    TPS_ALARM_SAME_IP, //IP地址冲突
    
    TPS_ALARM_TST_NO=60,//无异常
    TPS_ALARM_TST_DISKFULL,//硬盘满
    TPS_ALARM_TST_DISKERROR,//硬盘错误
    TPS_ALARM_TST_RETICLEDISCONNECT,//网线断
    TPS_ALARM_TST_IPCONFLICT,//IP冲突
    TPS_ALARM_TST_ILLEGEACCESS,//非法访问
    TPS_ALARM_TST_VIDEOSTANDARDEXCEPTION,//制式异常
    TPS_ALARM_TST_VIDEOEXCEPTION,//视频异常
    TPS_ALARM_TST_ENCODEERROR,//编码异常
    TPS_ALARM_TST_TST_NO,//无报警
    TPS_ALARM_TST_TST_IN,//报警输入
    TPS_ALARM_TST_TST_MOTION,//移动侦测
    TPS_ALARM_TST_TST_VIDEOLOSS,//视频丢失
    TPS_ALARM_TST_TST_EXCEPION,//异常报警
    TPS_ALARM_TST_TST_MASK,//遮挡检测
    
};

typedef struct{
    char szDevId[VS_DEV_ID_LEN];//设备ID
    unsigned int nTimestamp;//报警时间
    unsigned int nType;//报警类型
    unsigned int nIsRaise;//报警状态，产生 or 消失
    unsigned int nAlarmLevel;//报警级别
    char szDesc[128];//报警描述
    int  nChannelId;
}TPS_AlarmInfo;

//事件类型定义
enum TPS_EVENT_TYPE
{
    TPS_EVENT_NONE = 0,
    TPS_EVENT_USB_PLUGIN,//U盘插入
    TPS_EVENT_USB_UNPLUG,//U盘拔出
    TPS_EVENT_SD1_PLUGIN,//SD1插入
    TPS_EVENT_SD1_UNPLUG,//SD1拔出
    TPS_EVENT_SD2_PLUGIN,//SD2插入
    TPS_EVENT_SD2_UNPLUG,//SD2拔出
    TPS_EVENT_RECORD_START,//录像开始
    TPS_EVENT_RECORD_FINISHED,//录像结束
    TPS_EVENT_RECORD_FILE_REMOVED,//录像文件删除
    TPS_EVENT_RECORD_DIR_REMOVED,//录像目录删除
    TPS_EVENT_START_UPDATE_FIRMWARE,//固件更新
    TPS_EVENT_UPDATE_FIRMWARE_OK,//固件更新成功
    TPS_EVENT_UPDATE_FIRMWARE_FAILED,//固件更新失败
    TPS_EVENT_UPDATE_CONFIG_OK,//配置更新
    TPS_EVENT_UPDATE_CONFIG_FAILED,//配置更新失败
    TPS_EVENT_REBOOT,//重启
    TPS_EVENT_GPS_INFO,//GPS信息
    TPS_EVENT_OTHER,//其他
};

typedef struct{
    char szDevId[VS_DEV_ID_LEN];//设备ID
    unsigned int nTimestamp;//事件时间
    unsigned int nType;//事件类型
    char szDec[128];//事件描述
}TPS_EventInfo;


enum TPS_ERR_NUM
{
	ERR_NONE = 0,
	ERR_OUTOFF_MEMORY = -100, //申请内存失败  
	ERR_INVALID_ADDR = -101,//服务器地址错误  
	ERR_SOCKET = -102,//网络异常  
	ERR_NOT_FIND_DEV = -103,//未找到此设备  
	ERR_DEV_LOCK = -104,//设备被锁定  
	ERR_USER_PASSWORD = -105,//用户名，密码错误  
	ERR_RTSP_REALPLAY = -106,//RTSP播放失败  
	ERR_RTSP_STOPPLAY = -107,//RTSP停止失败  
	ERR_INVALID_XML = -108,//XML数据错误  
    ERR_P2P_SVR_LOGIN_FAILED = -109,//播放失败,P2P服务器登录失败  
    ERR_P2P_DISCONNECTED = -110,// 播放请求中，正在建立P2P连接   
	ERR_P2P_AUTH_FAILED = -111,//播放失败，P2P设备认证失败  
	ERR_UPNP_DISCONNECT = -112,//播放失败， UPNP设备连接失败  
	ERR_UPNP_DEV_AUTH_FAILED = -113,//播放失败， UPNP设备认证失败  
    ERR_PLAY_FAILED = -114, //播放失败，未知错误请重新播放  
    ERR_AUDIO_NOT_START = -115,//对讲没启动  
    ERR_AUTHCODE_NULL=-116,//请求参数Authcode为空  
    ERR_INVALID_SESSION=-117,//用户登陆session无效  
    ERR_INVALID_RANDOM_DATA=-118,//随机数失效  
    ERR_RSP_TIMEOUT=-119,//响应超时  
    ERR_P2P_DEV_NOT_ALLOW_REPLAY=-120,//设备不支持前端回放  
    ERR_NVR_CHANNEL_OFFLINE=-121,//nvr channel is offline
    ERR_USER_NOT_FIND=-122,//用户不存在  
    ERR_UNKOWN = -200,// 未知错误 
    ERR_NO_INIT_LIB = -201,
    ERR_INVALID_PARAMETER = -202,
    ERR_USER_NOT_LOGIN = -203,
    ERR_LIB_FREE = -204,
    ERR_BUFFER_IS_SMALL = -205,
    ERR_DATA_FORMAT_ERROR = -206,
    ERR_SAVE_DATA_FAILED = -207,
    ERR_NO_MORE_DATA = -208,
};

//音频对讲数据
typedef struct
{
	int len;
	unsigned int frame_timestamp;//此帧对应的时间戳，用于音视频同步，一帧中的不同包时间戳相同
	unsigned short pack_seq;//包序号-65535，到最大后从开始
	unsigned int  frame_index; //新增加字段
	char * pBuffer;
}TPS_AudioData;

//云台控制枚举
enum PTZ_CMD_TYPE
{
	LIGHT_PWRON=2,// 2 接通灯光电源
	WIPER_PWRON,// 3 接通雨刷开关
	FAN_PWRON,// 4 接通风扇开关
	HEATER_PWRON,// 5 接通加热器开关
	AUX_PWRON1,// 6 接通辅助设备开关
	AUX_PWRON2,// 7 接通辅助设备开关
	ZOOM_IN_VALUE= 11,// 焦距变大
	ZOOM_OUT_VALUE, //12 焦距变小
	FOCUS_NEAR, //13 焦点前调
	FOCUS_FAR, //14 焦点后调
	IRIS_OPEN, //15 光圈扩大
	IRIS_CLOSE,// 16 光圈缩小
	TILT_UP,// 17 云台上
	TILT_DOWN, //18 云台下
	PAN_LEFT,// 19 云台左
	PAN_RIGHT,// 20 云台右
	UP_LEFT,// 21 云台左上
	UP_RIGHT,// 22 云台右上
	DOWN_LEFT, //23 云台左下
	DOWN_RIGHT,//24 云台右下
	PAN_AUTO,//25 云台自动
	STOPACTION//26 停止
};

enum{
    error_no = 0,
    error_invalid_ip,
    error_socket_init,
    error_socket_create,
    error_socket_set_recv_buf,
    error_socket_connect_delay,
    error_socket_send_fail,
    error_socket_read_delay,
    error_socket_recv_fail,
    error_no_recv_xml,
    error_out_buffer_too_small,
    error_out_buffer_invalid,
    error_dev_sn,
    error_invalid_username_or_password
};

typedef void (*fcLogCallBack)(unsigned int uLevel, const char* lpszOut);
typedef int (*MsgRspCallBack)(unsigned int nMsgType, char* pData, unsigned int nDataLen);
typedef int (*MediaRecvCallBack)(char* pDevId, unsigned int nMediaType, unsigned char* pFrameData, unsigned int nDataLen, TPS_EXT_DATA *pExtData);


/*************************************************************************************************************************
*                                             功能接口函数与说明                                                            *
**************************************************************************************************************************/
/*
 函数名称：FC_init
 函数功能：初始化库接口
 参数说明：空
 返回值：0：成功，非0:失败
 */
int FC_init(void);

/*
 函数名称：FC_initEx
 函数功能：初始化库接口，不可以在回调线程中直接调用，会造成死锁
 参数说明：nNetType:网络类型，0表示WIFI/4G，1表示3G, 2表示2G
 返回值：0：成功，非0:失败
 */
int FC_initEx(int nNetType);

/*
 函数名称：FC_Free
 函数功能：释放库接口
 参数说明：空
 返回值：0:成功，非0:失败
 */
int FC_Free(void);

/*
 函数名称：FC_SetfcLogCallBack
 函数功能：设置日志回调函数，用来接收库返回的日志
 参数说明：回调函数
 返回值：0:成功，非0:失败
 */

int FC_SetfcLogCallBack(fcLogCallBack fMsgRspCallBack);
int FC_SetfcLogCallBackEx(fcLogCallBack fMsgRspCallBack,int nLevel);
int FC_SetfcLogPutAddr(const char *addr, int port);
int FC_Log(int level, const char *msg);


/*
 函数名称：FC_SetMsgRspCallBack
 函数功能：设置回调函数，用来接收库返回的消息通知和数据
 参数说明：回调函数
 返回值：0:成功，非0:失败
 */
int FC_SetMsgRspCallBack(MsgRspCallBack fMsgRspCallBack);

/*
 函数名称：FC_SetMediaRecvCallBack
 函数功能：设置回调函数，用来接收媒体数据
 参数说明：回调函数
 返回值：0:成功，非0:失败
 */
int FC_SetMediaRecvCallBack(MediaRecvCallBack fMediaRecvCallBack);

/*
 函数名称：FC_Login
 函数功能：登陆服务器
 参数说明：pUserName：登陆用户名；pPwd：登陆密码；pVmsIp：服务器地址；nVmsPort：服务器端口
 返回值：0：成功，非0:失败
 */
int FC_Login(const char* pUserName, const char* pPwd, const char* pVmsIp, unsigned short nVmsPort);

/*
 函数名称：FC_UpdateDevList
 函数功能：更新设备列表
 参数说明：无
 返回值：0：成功，非0:失败
 注意：此接口只针对按用户登录方式更新
 */
int FC_UpdateDevList(void);

/*
 函数名称：FC_Logout
 函数功能：注销登录，不可以在回调线程中直接调用，会造成死锁
 参数说明：空
 返回值：0:成功；非0:失败
 */
int FC_Logout(void);

/*
 函数名称：FC_AddWatch
 函数功能：请求某设备视频流
 参数说明：pDevId：设备id；nStreamNo：请求该设备的哪一路流,0:主码流，1:子码流; nFrameType:0表示请求播放所有视频帧，1表示只请求播放关键帧
 返回值：0:成功；非0:失败
*/
int FC_AddWatch(char* pDevId, int nStreamNo, int nFrameType);

/*
 函数名称：FC_StopWatch
 函数功能：停止某设备视频流
 参数说明：pDevId：设备id
 返回值：0:成功；非0:失败
 */
int FC_StopWatch(char* pDevId);

/*
 函数名称：FC_StartRecord
 函数功能：开始录像
 参数说明：pDevId：设备id; pFilePath:录像文件存储路径(注意如果传入的是.mp4结尾的路径文件则只生成此文件，不会自动切片); nFileMaxSeconds:单个录像文件时长，单位为秒
 返回值：0:成功；非0:失败
 */
int FC_StartRecord(char* pDevId, char *pFilePath, int nFileMaxSeconds);

/*
 函数名称：FC_StopRecord
 函数功能：停止录像
 参数说明：pDevId：设备id
 返回值：0:成功；非0:失败
 */
int FC_StopRecord(char* pDevId);


/*
 函数名称：FC_PTZAction
 函数功能：云台控制
 参数说明：pDevId：设备id;pPtzCmd:云台控制命令，具体命令请参考文档说明
 返回值：0:函数调用成功；非0:失败
 说明：此接口由用户自己编写云台控制命令传入是方便用户传输自己是私有数据
 */
int FC_PTZAction(char* pDevId, char* pPtzCmd);

/*
 函数名称：FC_PTZAction
 函数功能：云台控制
 参数说明：pDevId：设备id;nPTZConmand:云台控制命令，参考枚举值，nPSpeed：水平速度；nTspeed：垂直速度
 返回值：0:函数调用成功；非0:失败
 */
int FC_PTZActionEx(char *pDevId, int nPTZConmand, int nPSpeed, int nTspeed);

/*
 函数名称：FC_StartTalk
 函数功能：请求对讲
 参数说明：pDevId：设备id；bCaptureAudio：是否由SDK内部直接采集音频，如果外面自行采集编码则调用FC_InputAudioData输入。
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_StartTalkEx(char* pDevId, bool bCaptureAudio);
int FC_StartTalk(char* pDevId);

/*
 函数名称：FC_InputAudioData
 函数功能：传入对讲数据,调用此函数的前提的FC_StartTalk对讲启动成功。
 参数说明：pDevId: 设备ID，TPS_AudioData：采集编码好的数据
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_InputAudioData(char*pDevId, TPS_AudioData oAudioData);

/*
 函数名称：FC_StopTalk
 函数功能：停止对讲
 参数说明：pDevId：设备id
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_StopTalk(char* pDevId);

/*
 函数名称：FC_GetRegImg
 函数功能：获取云服务器注册验证码
 参数说明：pFileName：验证码文件路径和名名称，如：路径/regimg.jpg
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_GetRegImg(char* pFileName);

/*
 函数名称：FC_GetRegNumber
 函数功能：获取手机或者邮箱注册验证码
 参数说明：pPhoneMail：对应注册的手机号码或者注册的邮箱地址；pLang：注册验证语言，支持zh-cn, zh-tw, en-us
 返回值：0:函数调用成功；非0：函数调用失败，错误码如下：
 */
enum{
    get_reg_number_error_null = 0,
    get_reg_number_error_param,//参数错误，比如手机号码不正确，邮箱不正确
    get_reg_number_error_quik,//一分钟内重复发送多次，发送间隔时间要大于1分钟
    get_reg_number_error_sendmsg,//发送手机短信失败，可能是手机号错误，或短信发送接口错误
    get_reg_number_error_phonemail_used,//手机或者邮箱已经被注册过
    get_reg_number_error_user_not_find,//用户账号不存在
    get_reg_number_error_user_phone,//用户账号和手机号码／邮箱地址不匹配
    get_reg_number_error_other,
};
int FC_GetRegNumber(char *pPhoneMail, char *pLang);

/*
 函数名称：FC_GetResetRegNumber
 函数功能：获取重置密码的验证码
 参数说明：pPhoneMail：对应注册的手机号码或者注册的邮箱地址；pUser:新账号对应pPhoneMail值,旧账号对应旧账号值;pLang：注册验证语言，支持zh-cn, zh-tw, en-us
 返回值：0:函数调用成功；非0：函数调用失败，错误码参考FC_GetRegNumber定义：
 */
int FC_GetResetRegNumber(char *pPhoneMail, char *pUser, char *pLang);

/*
 函数名称：FC_ResetUserPassword
 函数功能：重置用户密码
 参数说明：pPhoneMail：对应注册的手机号码或者注册的邮箱地址；pUser:新账号对应pPhoneMail值,旧账号对应旧账号值; pPassword:重置密码; pCode:验证码（FC_GetResetRegNumber获取）; pLang：注册验证语言，支持zh-cn, zh-tw, en-us
 返回值：0:函数调用成功；非0：函数调用失败，错误码参考定义：
 */
enum{
    reset_psw_error_null=0,
    reset_psw_error_user_empty,//用户账号为空
    reset_psw_error_code_valid,//验证码过期或未成功发送验证码
    reset_psw_error_code,//验证码错误
    reset_psw_error_user,//用户账号和手机号码／邮箱地址不匹配
};
int FC_ResetUserPassword(char *pPhoneMail, char *pUser, char *pPassword, char *pCode, char *pLang);

/*
 函数名称：FC_RegCSUser
 函数功能：云服务器用户注册
 参数说明：pUserName：用户名；pPassword：用户密码；pMail：邮箱（非必填）；pPhone（非必填）；pCode：验证码(FC_GetRegImg函数获取到的图片显示数字)
 返回值：0:函数调用成功；非0:函数调用失败，错误码如下
 */
enum{
    reg_error_null = 0,
    reg_error_user,
    reg_error_password,
    reg_error_code,
    reg_error_user_length,
    reg_error_psw_length,
    reg_error_mail,
    reg_error_phone,
    reg_error_user_exist,
    reg_error_other,
};

int FC_RegCSUser(char *pUserName, char *pPassword, char *pMail, char *pPhone, char *pCode);

int FC_RegCSUserEx(char *pUserName, char *pPassword, char *pMail, char *pPhone, char *pCode);

/*
 函数名称：FC_AddDevice
 函数功能：为注册用户添加设备
 参数说明：pDevId：设备ID；pDevUser：设备账号；pDevPassword：设备密码
 返回值：0:函数调用成功；非0:函数调用失败，错误码如下
 注意：此函数必须是按用户账号登陆云服务器后才调用此函数为该用户添加设备
 */
enum  {
    ad_error_null = 0,
    ad_error_notlogin,
    ad_error_id,
    ad_error_dev_exist,
    ad_error_dev_lock,
    ad_error_user_psw,
    ad_error_other,
    };

int FC_AddDevice(char* pDevId, char *pDevUser, char *pDevPassword);

/*
 函数名称：FC_DelDevice
 函数功能：删除用户账号下添加的设备
 参数说明：pDevId：设备ID
 返回值：0:函数调用成功；非0:函数调用失败，错误码如下
 注意：此函数必须是按用户账号登陆云服务器后才调用此函数删除用户添加的设备
 */
enum  {
    del_error_null = 0,
    del_error_notlogin,
    del_error_id,
    del_error_connect_failed,
    del_error_user_disabled,
    del_error_user_no_auth,
    del_error_other,
};

int FC_DelDevice(char* pDevId);

int FC_GetDeviceNetworkQSInfo(char* pDevId);

/*
 函数名称：FC_ModifyDevName
 函数功能：修改设备备注名
 参数说明：pDevId：设备ID；pDevNewName：设备备注名称；nLoginByUser：登陆方式，按用户登陆填1，按设备ID登陆填0
 返回值：0:函数调用成功；非0:函数调用失败，错误码如下
 注意：设备的备注名如果是中文必须是utf-8编码
 */
enum  {
    md_error_null = 0,
    md_error_id_null,
    md_error_name_null,
    md_error_dev_notfind,
    md_error_user_psw,
    md_error_connect_failed,
    md_error_user_not_login,
    md_error_other,
};

int FC_ModifyDevName(char* pDevId, char *pDevNewName, int nLoginByUser);
/*
 函数名称：FC_ModifyDevPassword
 函数功能：更新修改后的设备密码
 参数说明：pDevId：设备ID; pDevName:修改后的设备账号; pDevPswd：修改后的设备密码
 返回值：0:函数调用成功。非0失败共用上面错误码
 注意：只有确认设备密码修改成功后才能调用此接口更新，否则会导致该设备无法登陆。
 */
int FC_ModifyDevPassword(char* pDevId, char *pDevUser, char *pDevPswd);

/*
 函数名称：FC_StopDevCom
 函数功能：停止与设备之间的通信
 参数说明：pDevId：设备ID;
 返回值：0:函数调用成功。非0失败
 */
int FC_StopDevCom(char *pDevId);

/*
 函数名称：FC_StopDevComEx
 函数功能：停止所有设备之间的通信
 参数说明：
 返回值：0:函数调用成功。非0失败
 */
int FC_StopDevComEx(void);

/*
 函数名称：FC_ResumeDevCom
 函数功能：唤醒设备通信
 参数说明：
 返回值：0:函数调用成功。非0失败
 */
int FC_ResumeDevCom(void);


/*
 函数名称：FC_GetP2PDevConfig
 函数功能：读取设备配置
 参数说明：pDevId：设备ID nCommand：配置对应信息，请参考文档; pXml:xml文本内容
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回，如果成功则回调函数错误标记为0，消息类型（注意后面24位的值）与nCommand相同。
 */
int FC_GetP2PDevConfig(char* pDevId, int nCommand, char *pXml);

/*
 函数名称：FC_SetP2PDevConfig
 函数功能：设置设备配置
 参数说明：pDevId：设备ID; nCommand：配置对应信息，请参考文档; pXml:配置xml文本内容
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回，如果成功则回调函数错误标记为0，消息类型（注意后面24位的值）与nCommand相同。
 */
int FC_SetP2PDevConfig(char* pDevId, int nCommand, char *pXml);

/*
 函数名称：FC_P2PDevSystemControl
 函数功能：对设备进行高级系统控制
 参数说明：pDevId：设备ID nCommand：配置对应信息，请参考文档; pXml:配置xml文本内容
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回，如果成功则回调函数错误标记为0，消息类型（注意后面24位的值）与nCommand相同。
 */
int FC_P2PDevSystemControl(char* pDevId, int nCommand, char *pXml);

/*
 函数名称：FC_SetAutoRecvAlm
 函数功能：设置设备是否自动接收告警（不处于播放状态的时候也能接收告警）
 参数说明：pDevId：设备ID; nRecvAlm: 1表示不处于播放状态的时候也能自动接收告警，0：表示不处于播放状态的时候不自动接收告警
 返回值：0:函数调用成功
 */
int FC_SetAutoRecvAlm(char* pDevId, int nRecvAlm);

/*
 函数名称：FC_SearchDevAlarm
 函数功能：按设备ID查询云服务器P2P设备报警信息
 参数说明：pDevId：设备ID; nPage: 查询的当前页，从1开始计算；nPageSize：每页多少条记录，取值范围10-40；
         pSTime:开始时间2013-03-06+01:01:01 ; pETime:结束时间2013-03-06+08:01:01
 返回值：0:函数调用成功，具体告警记录通过回调函数返回
 注意: 当前不支持跨天查询，只能选择指定日期不同时间段查询
 */
enum  {
    sa_error_null = 0,
    sa_error_id_null,
    sa_error_dev_notfind,
    sa_error_no_right,
    sa_error_page,
    sa_error_time,
    sa_error_other,
};
int FC_SearchDevAlarm(char* pDevId, int nPage, int nPageSize, char *pSTime, char *pETime);

/*
 函数名称：FC_SearchUserDevAlarm
 函数功能：按用户查询云服务器P2P设备报警信息
 参数说明：nPage: 查询的当前页，从1开始计算；nPageSize：每页多少条记录，取值范围10-40；
 pSTime:开始时间2013-03-06+01:01:01 ; pETime:结束时间2013-03-06+08:01:01
 返回值：0:函数调用成功，具体告警记录通过回调函数返回
 注意: 当前不支持跨天查询，只能选择指定日期不同时间段查询
 */

int FC_SearchUserDevAlarm(int nPage, int nPageSize, char *pSTime, char *pETime);

/*
 函数名称：FC_SearchDevAlarm
 函数功能：查询云服务器P2P设备最新报警信息
 参数说明：pDevId：设备ID;nPageSize: 获取最新指定数量pagesize告警信息
 返回值：0:函数调用成功，具体告警记录通过回调函数返回
 注意: 当前不支持跨天查询，只能选择指定日期不同时间段查询
 */
int FC_GetDevNewAlarm(char* pDevId, int nPageSize);

/*
 函数名称：FC_ReplayDeviceFile
 函数功能：回放前端设备录像文件
 参数说明：pDevId:设备ID；filenme:录像文件，通过FC_P2PDevSystemControl函数调用1021命令查询获得。例如：/mnt/mmc0/motion/2014/8/20/102222-av-1.avi
 返回值：0:函数调用成功，媒体参数和数据通过MediaRecvCallBack回调函数返回，类似实时流播放。
 */
int FC_ReplayDeviceFile(char *pDevId, char *filenme);

/*
 函数名称：FC_ControlReplay
 函数功能：前端设备录像文件回放控制
 参数说明：pDevId:设备ID；nAction:回放控制操作类型，参考枚举值；nParam:控制参数，当操作类型为ACTION_FAST,ACTION_SLOW时，此值为播放倍数。当操作类型为ACTION_SEEK时，表示要播放到第几秒。
 返回值：0:函数调用成功,非零失败
 */
enum REPLAY_IPC_ACTION
{
	ACTION_PLAY=0,
	ACTION_PAUSE,
	ACTION_RESUME,
	ACTION_FAST,
	ACTION_SLOW,
	ACTION_SEEK,
	ACTION_FRAMESKIP,
	ACTION_STOP,
	ACTION_CAPIMG=10,
	ACTION_CHANGE_SOUND,
	ACTION_RECV_DECODEPARAM,
};
int FC_ControlReplay(char *pDevId, int nAction, int nParam);

/*
 函数名称：FC_GetDevPicture
 函数功能：告警发生时客户端向摇头机获取抓拍的图片
 参数说明：pDevId：设备ID; nStreamNo:获取设备哪一路码流的图片,0:主码流，1:子码流; pSavePathFile:存储路径文件名称，jpg格式，如：/users/xx/1.jpg
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_GetDevPicture(char* pDevId, int nStreamNo, char *pSavePathFile);

/*
 函数名称：FC_GetDevNatType
 函数功能：获取设备的NATTYPE值
 参数说明：pDevId：设备ID;
 返回值：nattype <=0 获取失败； =16表示转发，>0&!=16表示P2P
 */
int FC_GetDevNatType(char *pDevId);

/*
 函数名称：FC_P2PSearchNvrRecByTime
 函数功能：查询nvr录像
 参数说明：pDevId：设备ID;pDate：查询日期，例如：“20151015”
 返回值：0：成功，！＝0：失败
 注意：查询结果通过消息通知返回，用1440个字符来表示，如：“ABCABC....”,'A':表示定时录像；‘B’:表示报警录像，‘C’:表示手动录像
 */
int FC_P2PSearchNvrRecByTime(char *pDevId, char* pDate);

/*
 函数名称：FC_P2PNvrReplayByTime
 函数功能：按时间播放nvr录像
 参数说明：pDevId：设备ID;pDateTime：播放时间点，例如：“201510151140”
 返回值：0：成功，！＝0：失败
 注意：媒体数据通过回调函数返回
 */
int FC_P2PNvrReplayByTime(char *pDevId, char* pDateTime);

/*
 函数名称：FC_ControlNVRReplay
 函数功能：nvr回放控制
 参数说明：pDevId：设备ID;lAction：控制命令，参考枚举值，lSpeed：速度；pPlayTime：seek控制命令时的播放时间点，例如：“201510151140”
 返回值：0：成功，！＝0：失败
 */
enum REPLAY_NVR_ACTION
{
    NVR_ACTION_RESUME=1,
    NVR_ACTION_PAUSE,
    NVR_ACTION_STOP,
    NVR_ACTION_FAST,
    NVR_ACTION_SLOW,
    NVR_ACTION_SEEK,
    NVR_ACTION_FRAMESKIP,
    NVR_ACTION_NORMAL,
};
int FC_ControlNVRReplay(char *pDevId, unsigned int lAction, unsigned int lSpeed, char *pPlayTime);

/*
 函数名称：FC_SearchIpType
 函数功能：查询ip归属运营商
 参数说明：ip：ip地址;
 返回值：>0：成功 <0：失败; 返回ip归属运营商包含：电信(1)、长宽(2)、联通(3)、铁通(4)、移动(5)、教育网(6)、未知(16)
 */
int FC_SearchIpType(char* ip);


/*
 函数名称：FC_UploadFile
 函数功能：上传文件（日志）到服务器
 参数说明：pPathfile：文件路径();
 返回值：=0：成功, !=0：失败;
 */
enum  {
    uf_error_null = 0,
    uf_error_param,
    uf_error_open_file,
    uf_error_memory,
    uf_error_socket,
};
int FC_UploadFile(char* pPathfile,char *pFileName);

/*
 函数名称：FC_SetTryP2PTimeout
 函数功能：设置p2p打洞超时时间（s），不设置默认是6秒。
 参数说明：nTime：设置范围>0,<3600;
 返回值：=0：成功, !=0：失败;
 */
int FC_SetTryP2PTimeout(int nTime);

/*
 函数名称：FC_ForceSubStreamByRelay
 函数功能：走转发的时候是否强制播放子码流，默认是强制播放子码流
 参数说明：bForce：true 强制播放子码流；false 不强制播放子码流
 返回值：=0：成功, !=0：失败;
 */
int FC_ForceSubStreamByRelay(bool bForce);

/*
 函数名称：FC_GetUpdateFWInfo
 函数功能：获取设备固件更新信息
 参数说明：pDevId:[in]设备id；pDevIdentify:[in]设备固件版本唯一标识，通过调用FC_P2PDevSystemControl接口取得；
 返回值：=0：成功, !=0：失败;
 */
enum
{
    gf_error_null = 0,
    gf_error_invalid_param,//无效参数
    gf_error_param_format,//参数格式错误
    gf_error_search_data,//数据查询错误
    gf_error_network,//网络异常
    gf_error_other,//未知
};
int FC_GetUpdateFWInfo(char *pDevId, char *pDevIdentify, char* capability);

/*
 函数名称：FC_AddFeedback
 函数功能：添加反馈信息
 参数说明：info:[in]反馈信息内容；phone:[in]联系方式; app_var: app版本号
 返回值：=0：成功, !=0：失败;
 */
int FC_AddFeedback(const char *info, const char *phone, const char *app_ver);

/*
 函数名称：FC_GetServiceMessage
 函数功能：获取运营消息
 参数说明：lang 消息语言, (0:中文, 1:英文)
 返回值：0:函数调用成功，具体消息记录通过回调函数返回,具体xml格式说明如下：
<xml><ret>0</ret><error></error>
     <ls><ID>9</ID><Title>11111</Title><Url>http://baidu.com</Url><AddTM>2016-07-05 17:52:53</AddTM><Img> http://seetong.com/logo.jpg</Img><gourl>http://www.seetong.com/client/gourl.php?id=9</gourl> </ls>
     <ls><ID>8</ID><Title>11111</Title><Url> http://baidu.com </Url><AddTM>2016-07-05 17:52:47</AddTM><Img> http://seetong.com/logo.jpg</Img><gourl>http://www.seetong.com/client/gourl.php?id=8</gourl> </ls>
     <ls><ID>7</ID><Title>11111</Title><Url> http://baidu.com</Url><AddTM>2016-07-05 17:52:44</AddTM><Img>http://seetong.com/logo.jpg</Img><gourl>http://www.seetong.com/client/gourl.php?id=7</gourl></ls>
</xml>
 */
int FC_GetServiceMessage(int lang);

/*
 函数名称：FC_AddCommentInfo
 函数功能：添加运营消息评论
 参数说明：info:[in]评论内容；pid:[in]运营消息的ID
 返回值：=0：成功, !=0：失败;
 */
int FC_AddCommentInfo(const char *info, const char *pid);

/*
 函数名称：FC_GetCommentInfo
 函数功能：获取运营消息评论
 参数说明：pid:[in]运营消息的ID, page：表示查询第几页，page_size：每页多少条记录
返回值：0:函数调用成功，具体消息记录通过回调函数返回,具体xml格式说明如下：
<xml><ret>0</ret><error></error><page>0</page><allpage>1</allpage><allcount>3</allcount>
     <ls><ID>1</ID><UserName>111111</UserName><Msg>111</Msg><AddTM>2016-08-04 09:58:31</AddTM><PubID>48</PubID><CanShow>1</CanShow></ls>
     <ls><ID>3</ID><UserName>测试</UserName><Msg>2341234123</Msg><AddTM>2016-08-04 10:11:21</AddTM><PubID>48</PubID><CanShow>1</CanShow></ls>
     <ls><ID>5</ID><UserName>zfuwen</UserName><Msg>2222</Msg><AddTM>2016-08-04 12:00:06</AddTM><PubID>48</PubID><CanShow>1</CanShow></ls>
</xml>
 */
int FC_GetCommentInfo(const char *pid, int page, int page_size);

const char* FC_GetSdkVersion(void);

/*
 函数名称：FC_SetUserInfo
 函数功能：设置当前登录用户信息
 参数说明：uname:[in]用户名；pwd:[in]密码
 返回值：=0：成功, !=0：失败;
 */
int FC_SetUserInfo(const char *uname, const char *pwd);

/*************************************************************************************************************************
 *                                   按用户登陆社交功能新增接口
 **************************************************************************************************************************/

/*
 函数名称：FC_AddFriend
 函数功能：请求添加好友
 参数说明：pFriendName：好友数字ID（必须是在云服务器注册好的用户）;pCheckMsg:验证消息
 返回值：0:函数调用成功
 注意: 此函数只是请求添加好友，需要等好友确认同意后才算真正添加成功，通过读取消息接口可以得知结果
 */
enum  {
    af_error_null = 0,
    af_error_friend_not_login,
    af_error_friend_not_find,
    af_error_friend_null,
    af_error_friend_add,
    af_error_netword,
    af_error_other,
};
int FC_AddFriend(char* pFriendName, char* pCheckMsg);

/*
 函数名称：FC_DelFriend
 函数功能：删除好友
 参数说明：pFriendName：好友账号（必须是在云服务器注册好的用户）
 返回值：0:函数调用成功
 */
enum  {
    df_error_null = 0,//成功
    df_error_friend_invalidate,//好友ID错误
    df_error_not_friend,//此ID不是你的好友
    df_error_netword,//链接失败
    df_error_other,//未知错误
};
int FC_DelFriend(char* pFriendName);

/*
 函数名称：FC_ConfirmAddFriend
 函数功能：接收/拒绝添加好友
 参数说明：pSid：系统通知消息中的sid; nAccept: 1表示接受，0表示拒绝添加好友
 返回值：0:函数调用成功
 */
enum  {
    ac_error_null = 0,
    ac_error_user_not_login,
    ac_error_auth_invalidate,
    ac_error_friend_null,
    ac_error_netword,
    ac_error_other,
};
int FC_ConfirmAddFriend(char* pSid, int nAccept);

/*
 函数名称：FC_GetFriendList
 函数功能：获取好友列表
 参数说明：nPage：第几页，0表示第一页; nPageSize: 每页几个好友，最大为500个
 返回值：0:函数调用成功，具体告警记录通过回调函数返回,具体xml格式说明如下：
 <xml>
 <ret>0</ret><error></error><allcount>0</allcount><page>0</page><pagesize>500</pagesize>
 <ls><fid>1000011</fid><fname>test1</fanme></ls>
 <ls><fid>1000012</fid><fname>test2</fanme></ls>
 <ls><fid>1000013</fid><fname>test3</fanme></ls>
 </xml>
 */
int FC_GetFriendList(int nPage, int nPageSize);

/*
 函数名称：FC_SendOfflineMsg
 函数功能：发送离线消息给好友
 参数说明：pFriendId：好友ID; pMsg: 消息内容，UTF-8编码
 返回值：<0:函数调用失败，>0函数调用成功，返回对应的发送消息ID
 */
enum  {
    sm_error_user_not_login=-10,
    sm_error_friend_invalidate,
    sm_error_friend_null,
    sm_error_msg_null,
    sm_error_network,
    sm_error_other,
};

long long FC_SendOfflineMsg(char* pFriendId, char *pMsg);

/*
 函数名称：FC_PutVideoMsg
 函数功能：推送视频消息给好友
 参数说明：pFriendId：好友ID; pMsg: 消息内容，UTF-8编码; pDevId:推送设备id; nTime:允许用户观看时间，0表示不限制时间，单位秒；
 返回值：<0:函数调用失败，>0函数调用成功，返回对应的发送消息ID
 */
long long FC_PutVideoMsg(char* pFriendId, char *pDevId, int nTime);

/*
 函数名称：FC_ReadOfflineMsg
 函数功能：读取离线消息
 参数说明：pFriendId：好友ID，-1表示所有好友; nPage: 第几页，0表示第一页；nPageSize: 表示每页几条消息，最大500条
         pSTime:消息开始发送时间点，为空表示不限制；pETime:消息结束发送时间点，为空表示不限制；
 返回值：0:函数调用成功，具体告警记录通过回调函数返回,xml格式说明如下：
 <xml>
 <ret>0</ret><error></error><allcount>0</allcount><page>0</page><pagesize>500</pagesize>
 <ls><id>1000011</id><tyid>10000</tyid><fname>test</fname><msg>信息1</msg><tm>2014-05-12 16:05:12</tm></ls>
 <ls><id>1000011</id><tyid>10000</tyid><fname>test</fname><msg>信息1</msg><tm>2014-05-12 16:05:12</tm></ls>
 <ls><id>1000011</id><tyid>10000</tyid><fname>test</fname><msg>信息1</msg><tm>2014-05-12 16:05:12</tm></ls>
 </xml>
 其中XML中，记录节点
 <ls><id>1000011</id><tyid>10000</tyid><fname>test</fname><msg>信息1</msg><tm>2014-05-12 16:05:12</tm></ls>
 意义如下：
 id:表示此消息唯一标识符,用于删除消息。
 tyid:表示消息类型，当前类型定义值如下：
 1	加好友消息提醒
 2	接受好友提醒
 3	拒绝好友提醒
 10000	好友离线消息
 fame为发送者的登录帐号名称,当类型为1，2，3时，此值固定为”统消息”
 msg为相关内容
 */
int FC_ReadOfflineMsg(char* pFriendId, int nPage, int nPageSize, char *pSTime, char *pETime);

/*
 函数名称：FC_DeleteOfflineMsg
 函数功能：删除离线消息
 参数说明：pMsgId: 消息id，-1表示删除所有消息; pFriendId: 好友id，-1表示所有好友
 返回值：0:函数调用成功,
 */
enum  {
    dm_error_null = 0,
    dm_error_user_not_login,
    dm_error_network,
    dm_error_other,
};
int FC_DeleteOfflineMsg(char* pMsgId, char* pFriendId);

/*
 函数名称：FC_LoginShareDev
 函数功能：登陆好友分享设备
 参数说明：pDevId：设备ID；pUser：登陆用户名；pPwd：登陆密码
 返回值：0：成功，非0:失败
 */
int FC_LoginShareDev(const char* pDevId, const char* pUser, const char* pPwd);

/**************************************************************************************************************************
                                    New function interface for Tomofun @2014-12-23
 *************************************************************************************************************************/


/*
 Function Name: FC_SettingDevWIFI()
 Description: Setting device WIFI ssid and password
 Parameters: pSSID@WIFI ssid name; pPassword@WIFI password; nComType@ 0:by AP, 1: by bluetooth
 Return: 0:success, !=0:failed
 Warring: you must connect device AP at first
 */
int FC_SettingDevWIFI(const char* pSSID, const char* pPassword, int nComType);

/*
 Function Name: FC_SettingDevWIFIEx()
 Description: Setting device WIFI ssid , password and message
 Parameters: pSSID@WIFI ssid name; pPassword@WIFI password; nComType@ 0:by WIFI AP, 1: by bluetooth; pAccessToken@device access token;pUserName@user info; pExtraInfo@extra info
 Return: 0:success, !=0:failed
 Warring: you must connect device AP at first
 */
int FC_SettingDevWIFIEx(const char *pSSID, const char *pPassword, int nComType, const char *pAccessToken, const char *pUserName, const char *pExtraInfo);

int FC_SettingDevWIFIUrlEnc(const char *pSSID, const char *pPassword, int nComType, const char *pAccessToken, const char *pUserName, const char *pExtraInfo);


/*
 Function Name: FC_LoginEx();
 Description: Login by deviceID, account, password, access_token;
 Parameters: pDevId@device id; pAccount@device login account; pPassword@device login password; pAccessToken@device access token;
 Return: 0:success, !=0:failed;
 */
int FC_LoginEx(const char *pDevId, const char *pAccount, const char *pPassword, const char *pAccessToken);

/*
 Function Name: FC_UpdateDevFirmware();
 Description: Update device firmware;
 Parameters: pDevId@device id; pFirmwareUrl@firmware url, such as: ftp://firmware/TH38C/xxxx.bin;
 Return: 0:success, !=0:failed;
 */
int FC_UpdateDevFirmware(const char *pDevId, const char *pFirmwareUrl);

/*
Function Name: FC_AddWatchEx();
Description: Get one device stream, can select communication mode;
Parameters: pDevId@device id; nStreamNo@stream type, 0:main-stream,1:sub-stream; nFrameType@0:get all video frame,1:only get key frame; nComType@select communication type, 0:by p2p, 1:by cloud relay;
Return: 0:success; !=0:failed;
 */
int FC_AddWatchEx(const char* pDevId, int nStreamNo, int nFrameType, int nComType);

/*
 Function Name: How to get sound level
 Description: If you receive audio frame form MediaRecvCallBack pls check TPS_EXT_DATA struct:
 typedef struct
 {
 int     bIsKey;
 double  timestamp;
 }TPS_EXT_DATA;
 
 If it is audio frame, we use bIsKey field descript sound level;
 
 */

/*
 Function Name: FC_FoodToss();
 Description: toss food
 Parameters: pDevId@ device id; nInterval@ inteval time
 Return: 0: success, !=0: failed
 */

int FC_FoodToss(const char *pDevId, int nInterval);


/*
 Function Name: FC_SetSoundFile();
 Description: send sound file to device;
 Parameters: pDevId@ device id; pSoundFile@ sound file, such as: /sound/file/aa.wav;
 Return: 0: success, !=0: failed;
 Warring: sound file size less than 3 seconds.
 */

int FC_SetSoundFile(const char *pDevId, const char *pSoundFile);


/*
 Function Name: FC_SetBarkingAlert();
 Description: Set barking alert
 Parameters: pDevId@ device id, nLevel@ sound level, nDuration@Duration time, nCount@up level times
 Return: 0: success, !=0: failed
 */

int FC_SetBarkingAlert(const char *pDevId, int nLevel, int nDuration, int nCount);

/*
 Function Name: FC_GetBarkingAlert();
 Description: Get barking alert
 Parameters: pDevId@ device id
 Return: 0: success, !=0: failed
 */

int FC_GetBarkingAlert(const char *pDevId);

/*
 Function Name: FC_SaveOneVideoFile();
 Description: save one video file to s3
 Parameters: pDevId@ device id, nTime@ video time rang:1-1440min
 Return: 0: success, !=0: failed
 */
int FC_SaveOneVideoFile(const char *pDevId, const char *pS3Url, int nTime);

/*
 Function Name: FC_PlayOneVideoFile();
 Description: play one video file from s3
 Parameters: pS3Url@ video file url from s3
 Return: 0: success, !=0: failed
 */
int FC_PlayOneVideoFile(const char *pS3Url);

/*
 Function Name: FC_PlaySoundFile();
 Description: play sound file
 Parameters: pDevId@ device id
 Return: 0: success, !=0: failed
 */
int FC_PlaySoundFile(const char *pDevId);

/*
 Function Name: FC_SpeakerVolume();
 Description: config speaker volume
 Parameters: pDevId@ device id; nVolume@ speaker volume: 0-100
 Return: 0: success, !=0: failed
 */
int FC_SpeakerVolume(const char *pDevId, int nVolume);

/*
 Function Name: FC_GetSpeakerVolume();
 Description: get config speaker volume
 Parameters: pDevId@ device id;
 Return: 0: success, !=0: failed; recv TPS_MSG_RSP_GET_SPEAKER_VOLUME
 */
int FC_GetSpeakerVolume(const char *pDevId);


/*
 Function Name: FC_SetHttpsProtocol();
 Description: set communication protocol by https
 Parameters:
 Return: 0: success, !=0: failed
 */
int FC_SetHttpsProtocol(void);


/*************************************************************************************************************************
 *                                   433模块外接报警器P2P项目新增接口
 **************************************************************************************************************************/
/*
 函数名称：FC_StartAlertorBind
 函数功能：客户端请求摇头机开始对码
 参数说明：pDevId：设备ID
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_StartAlertorBind(char* pDevId);

/*
 函数名称：FC_StopAlertorBind
 函数功能：客户端停止摇头机对码
 参数说明：pDevId：设备ID
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_StopAlertorBind(char* pDevId);

/*
 函数名称：FC_GetAlertorList
 函数功能：获取摇头机对码成功的报警器列表
 参数说明：pDevId：设备ID
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_GetAlertorList(char* pDevId);

/*
 函数名称：FC_DelAlertorBind
 函数功能：删除摇头机对码成功的报警器
 参数说明：pDevId：设备ID , pAlerAddr:报警器地址码
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_DelAlertorBind(char* pDevId, char *pAlerAddr);

/*
 函数名称：FC_SetSecurity
 函数功能：布撤防
 参数说明：pDevId：设备ID , pAlerAddr:报警器地址码, nEventType:报警器事件类型（1：报警；2：低压；3：心跳）; nEnable: 1 布防 0 撤防；
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_SetSecurity(char* pDevId, char *pAlerAddr, int nEventType, int nEnable);

/*
 函数名称：FC_GetSecurity
 函数功能：获取布撤防状态
 参数说明：pDevId：设备ID
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_GetSecurity(char* pDevId);

/*
 函数名称：FC_SetAlertorAlias
 函数功能：设置报警器别名
 参数说明：pDevId：设备ID , pAlerAddr:报警器地址码, pAlias:别名
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 注意：别名pAlias必须是16进制数据，如：sprintf(szAlias, "%02X", pAlias[i]).设置好的别名获取回来的别名也是16进制数据需要转换成对应的字符显示。
 */
int FC_SetAlertorAlias(char* pDevId, char *pAlerAddr, char *pAlias);

/*
 函数名称：FC_SetAlertorPreset
 函数功能：设置报警器别名
 参数说明：pDevId：设备ID , pAlerAddr:报警器地址码, nPreset:预置点
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_SetAlertorPreset(char* pDevId, char *pAlerAddr, int nPreset);

/*
 函数名称：FC_GetAlmPicture
 函数功能：告警发生时客户端向摇头机获取抓拍的图片
 参数说明：pDevId：设备ID , nAlmId:告警ID，在告警通知消息里有带 nPicIndex:图片索引号，用于获取那张图片（总共5张）,pSavePathFile:存储路径文件名称，jpg格式，如：/users/xx/1.jpg
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_GetAlmPicture(char* pDevId, char *pAlmId, int nPicIndex, char *pSavePathFile);

/*
 函数名称：FC_ConfirmAlertorAlm
 函数功能：确认告警，摇头机会停止相应的告警联动
 参数说明：pDevId：设备ID , pAlerAddr:报警器地址码
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回
 */
int FC_ConfirmAlertorAlm(char* pDevId, char *pAlerAddr);


/*************************************************************************************************************************
 *                                   OSS云存储新增接口@2014-11-05
 **************************************************************************************************************************/

enum
{
    oss_error_null=0,
    oss_error_dev_not_find,
    oss_error_dev_not_pay,
    oss_error_param_invalidate,
    oss_error_req_failed,
    oss_error_stop_failed,
    oss_error_search_failed,
    oss_error_download_failed,
    oss_error_del_failed,
    oss_error_set_replaypos_failed,
};


/*
 函数名称：FC_SearchOssObjectList
 函数功能：查询云存储服务器上某一天的录像文件
 参数说明：pDevId：设备ID , pDate：查询日期格式：20141105；nType：录像类型，0：定时录像，1：IO报警录像，2：移动侦测录像，3：图片
 返回值：0:函数调用成功；具体的响应结果都通过回调函数返回
 */
int FC_SearchOssObjectList(char* pDevId, char* pDate, int nType);

/*
 函数名称：FC_DownloadOssObject
 函数功能：下载云存储服务器上的录像文件
 参数说明：pDevId：设备ID , pObjectPathName：录像文件名称；pSavePathName：存储路径和文件
 返回值：0:函数调用成功；
 */
int FC_DownloadOssObject(char* pDevId, char* pObjectPathName, char* pSavePathName);

/*
 函数名称：FC_DeleteOssObject
 函数功能：查询云存储服务器上某一天的录像文件
 参数说明：pDevId：设备ID , pDate：查询日期格式：20141105；nType：录像类型，0：定时录像，1：报警录像，2：图片
 返回值：0:函数调用成功；具体的响应结果都通过回调函数返回
 */
int FC_DeleteOssObject(char* pDevId, char* pObjectPathName);


/*
 函数名称：FC_ReqOssObjectStream
 函数功能：请求获取某个object视频流
 参数说明：pDevId：设备ID ,pObjectPathName:录像文件，nObjectSize：录像文件大小；pIndexPathName：索引文件，nIndexSize：索引文件大小;nPlayPos:播放位置(单位秒)
 返回值：0:函数调用成功；媒体参数和数据与前端录像回放一样通过回调函数返回
 */
int FC_ReqOssObjectStream(char* pDevId, char* pObjectPathName, int nObjectSize, char *pIndexPathName, int nIndexSize, int nPlayPos);

/*
 函数名称：FC_StopOssObjectStream
 函数功能：停止获取某个object视频流
 参数说明：pDevId：设备ID
 返回值：0:函数调用成功
 */
int FC_StopOssObjectStream(char* pDevId);

/*
 函数名称：FC_SetOssObjectReplayPos
 函数功能：设置oss文件播放位置
 参数说明：pDevId：设备ID,nPlayPos:播放位置(单位秒)
 返回值：0:函数调用成功
 */
int FC_SetOssObjectReplayPos(char* pDevId, int nPlayPos);



/*************************************************************************************************************************
 *                                   设备直连访问相关定义
 **************************************************************************************************************************/


#define MAX_ALARM_DATA			 128
#define MAX_IP_NAME_LEN			 256
#define MAC_ADDRESS_LEN			 256
#define MAX_IPC_SERIALNUMBER	 32
#define MAX_DEVICETYPE_LEN_NETSDK		 128

#ifndef ZOOM_IN
#define ZOOM_IN		ZOOM_IN_VALUE
#define ZOOM_OUT	ZOOM_OUT_VALUE
#endif

enum ERROR_CODE
{
	ERR_WSASETUP_FAIL = -100,
	ERR_SSI_INIT_FAIL,
	ERR_SLOG_INIT_FAIL,
	ERR_CREATE_TASK_FAIL,
	ERR_PARAM_INVALID,
	ERR_XML_FORMAT_FAIL,
	ERR_PTZ_CMD_INVALID,
	ERR_UPLOAD_FILE_ERROR,
	ERR_UPLOAD_FILE_REFUSE,
	ERR_DOWNLOAD_FILE_REFUSE,
	ERR_SERIAL_NOT_START,
	ERR_NOT_ALLOW_REPLAY,
	ERR_DEV_NOT_CONNECTED,
	ERR_DEV_NOT_LOGIN,
	ERR_NOT_REPLAY_MODE,
	ERR_PLAY_ACTION,
	ERR_AUDIO_STARTED,
    
	ERR_NOT_FIND_DEVICE=-9000002,
	ERR_OPEN_AUDIOCAPTURE_FAIL,
	ERR_START_AUDIOCAPTURE_FAIL,
	ERR_AUDIO_PARAM_ERROR,
	ERR_MSGTYPE_ERROR,
	ERR_INIT_SOCKET_ERROR,
	ERR_PARAM_ERROR,
	ERR_NOT_DEV_EXIST,
	ERR_START_THREADERROR,
	ERR_NOT_FIND_STREAM,
	ERR_ISUPLOADING_ERROR,
	ERR_ISDOWNLOADING_ERROR,
	ERR_IS_STARTAUDIO_ERROR,
	ERR_ISFINISH_ERROR,
	ERR_NOT_DOWNLOAD_MODE_ERROR,
	ERR_PTZCMD_ACTION_ERROR,
	ERR_LOC_FILE_ERROR,
	ERR_NOT_REPLAY_MODE_ERROR,
	ERR_PLAY_ACTION_ERROR,
	ERR_NOT_ALLOW_REPLAY_ERROR,
	ERR_MEMORY_SIZE_ERROR,
	ERR_XML_FORMAT_ERROR,
	ERR_CREATE_SOCKET_ERROR,
	ERR_SEND_MODIFYCMD_ERROR,
	ERR_NOT_STARTTALK_MODE_ERROR,
	ERR_RECORD_MEDIA_PARAM_ERROR,
	ERR_RECORD_CREATEERROR,
	ERR_RECORD_ISRECORDING,
	ERR_RECORD_FILEMAXSECONDS_ERROR,
	ERR_RECORD_ALLRECORDSECONDS_ERROR,
	ERR_RECORD_NOTRUNNING,
	ERR_RECORD_STREAMPARAM_ERROR,
	ERR_RECORD_WRITETEMPBUFFER_ERROR,
	ERR_RECORD_ISNOTRECORDSTREAM_MODE,
	ERR_RECORD_NOTINPUTSTREAM_MODE,
	ERR_RECORD_FILEPATH_ERROR,
	ERR_DLL_NOINITORRELEASE_ERROR,
    ERR_NOT_FIND_FILEHANDLE,
    ERR_NOT_READMODE,
    ERR_OPEN_FILEERROR,
    ERR_MP4FILE_FORMAT_ERROR,
    ERR_READ_P2PNETWORK_ERROR,
};

enum enumNetSatateEvent
{
	EVENT_CONNECTING,
	EVENT_CONNECTOK,
	EVENT_CONNECTFAILED,
	EVENT_SOCKETERROR,
	EVENT_LoginOk,
	EVENT_LOGINFAILED,
	EVENT_STARTAUDIOOK,
	EVENT_STARTAUDIOFAILED,
	EVENT_STOPAUDIOOK,
	EVENT_STOPAUDIOFAILED,
	EVENT_SENDPTZOK,
	EVENT_SENDPTZFAILED,
	EVENT_SENDAUXOK,
	EVENT_SENDAUXFAILED,
	EVENT_UPLOADOK,
	EVENT_UPLOADFAILED,
	EVENT_DOWNLOADOK,
	EVENT_DOWNLOADFAILED,
	EVENT_REMOVEOK,
	EVENT_REMOVEFAILED,
	EVENT_SENDPTZERROR,
	EVENT_PTZPRESETINFO,
	EVNET_PTZNOPRESETINFO,
	EVENT_PTZALARM,
	EVENT_RECVVIDEOPARAM,
	EVENT_RECVAUDIOPARAM,
	EVENT_CONNECTRTSPERROR,
	EVENT_CONNECTRTSPOK,
	EVENT_RTSPTHREADEXIT,
	EVENT_URLERROR,
	EVENT_RECVVIDEOAUDIOPARAM,
	EVENT_LOGIN_USERERROR,
	EVENT_LOGOUT_FINISH,
	EVENT_LOGIN_RECONNECT,
	EVENT_LOGIN_HEARTBEAT_LOST,
	EVENT_STARTAUDIO_ISBUSY,
	EVENT_STARTAUDIO_PARAMERROR,
	EVENT_STARTAUDIO_AUDIODDISABLED,
	EVENT_CONNECT_RTSPSERVER_ERROR,
	EVENT_CREATE_RTSPCLIENT_ERROR,
	EVENT_GET_RTSP_CMDOPTION_ERROR,
	EVENT_RTSP_AUTHERROR,
	EVNET_RECORD_FILEBEGIN,
	EVENT_RECORD_FILEEND,
	EVENT_RECORD_TASKEND,
	EVENT_RECORD_DISKFREESPACE_TOOLOW,
	EVNET_RECORD_FILEBEGIN_ERROR,
	EVNET_RECORD_WRITE_FILE_ERROR,
	EVENT_RECORD_INITAVIHEAD_ERROR,
	EVENT_RECORD_MEDIA_PARAM_ERROR,
	EVENT_NVR_CHANNELS,
	EVENT_NVR_IPC_STATUS,
	EVENT_NVR_RECORD_DOWNLOAD_START,
	EVENT_NVR_RECORD_DOWNLOAD_FAILED,
	EVENT_NVR_RECORD_DOWNLOAD_STOP,
	EVENT_NVR_RECORD_DOWNLOAD_PROGRESS,
	EVENT_SYSTEMREBOOT_ANDRELoginOk,
	EVENT_NETWORKRESET_ANDRELoginOk,
	EVENT_UPLOAD_PROCESS,
	EVENT_DOWNLOAD_PROCESS,
	EVENT_DOWNLOAD_RETRY_ANDRESTAR,
	EVENT_LOGOUT_BYUSER,
	EVENT_P2P_CONNECT_STATE_INFO,
	EVNET_INITP2P_OK,
	EVNET_INITP2P_ERROR,
	EVENT_START_CONNECT_DEVICE,
	EVENT_START_CONNECT_DEVICE_ERROR,
	EVENT_P2PSERVER_LOGIN_OK,
	EVENT_P2PSERVER_LOGOUT,
	EVENT_P2PERROR_EVNETINFO,
	EVENT_P2PCONNECT_DEVICEOK,
	EVENT_P2PCONNECT_CLOSE,
	EVENT_P2P_EXIT_CONNECT,
	EVENT_CAPTURE_IMAGE_FINISH,
	EVENT_RECVABLITY_INFO,
	EVENT_P2P_CLINET_CHANNLEINFO,
	EVENT_P2P_STARTSTREAM_ERROR11,
    EVENT_P2P_STOPSTREAM_ERROR,
    EVENT_RECV_BAD_TPSMSG_ERROR,
};


enum PTZ_PRESET_TYPE
{
	SET_PRESET= 8 , //设置
	CLE_PRESET= 9, //删除
	GOTO_PRESET= 39 //查询
};

enum SEARCH_EVENT
{
	EVENT_SEARCH_RECV_NEWIPCINFO=1,//搜索设备时，找到新的设备
	EVENT_SEARCH_UPDATEINFO,//修改设备地址时，设备信息变化了
};

enum FILE_TYPE
{
	LOG_FILE,
	RECORD_FILE,
	CONFIG_FILE,
	UPDATE_FILE
};

typedef struct
{
	char  MACAddress[MAC_ADDRESS_LEN];
	int	  dhcpEnable;
	char  IPAddress[MAX_IP_NAME_LEN];
	char  netMask[MAX_IP_NAME_LEN];
	char  gateWay[MAX_IP_NAME_LEN];
	char  DNS1[MAX_IP_NAME_LEN];
	char  DNS2[MAX_IP_NAME_LEN];
}NetSDK_LANConfig;

typedef struct
{
	int auth;
	int videoPort;
	int rtpoverrtsp;
	int ptzPort;
	int webPort;
}NetSDK_StreamAccessConfig;

#define GROUP_NAME_MAX_LEN 32
#define ACCOUNT_STATUS_MAX_LEN 8
#define ACCOUNT_NAME_MAX_LEN 40
#define ACCOUNT_PASSWORD_MAX_LEN  40
#define MAX_ACCOUNT_COUNT 20

typedef struct
{
	char 	userName[ACCOUNT_NAME_MAX_LEN];
	char 	password[ACCOUNT_PASSWORD_MAX_LEN];
	char 	group[GROUP_NAME_MAX_LEN];
	char    status[ACCOUNT_STATUS_MAX_LEN];
}NetSDK_UserAccount;

typedef struct
{
	int count;
	NetSDK_UserAccount accounts[MAX_ACCOUNT_COUNT];
}NetSDK_UserConfig;

typedef struct
{
	char				ipc_sn[MAX_IPC_SERIALNUMBER];
	char				deviceType[MAX_DEVICETYPE_LEN_NETSDK];
	NetSDK_UserConfig			userCfg;
	NetSDK_StreamAccessConfig	streamCfg;
	NetSDK_LANConfig			lanCfg;
}NetSDK_IPC_ENTRY;


typedef struct __NetSDK_IPC_ENTRYV2
{
    char				ipc_sn[MAX_IPC_SERIALNUMBER];
    char				deviceType[MAX_DEVICETYPE_LEN_NETSDK];
    NetSDK_UserConfig			userCfg;
    NetSDK_StreamAccessConfig	streamCfg;
    NetSDK_LANConfig			lanCfg;
    //前面的为旧版本相关信息
    int					nFlag;//调用读取时，要使用后面的数据，必须检查此标记是否为0x01020304,否则读取不到后面的扩展数据
    int					nVersion;//调用读取时，版本号必须为1001,否则读不到云ID
    char				szCloudID[32];//云ID
}
NetSDK_IPC_ENTRYV2;



typedef struct _FRAMNE_INFO
{
	int bIsVideo;
	int bIsKeyFrame;
	double TimeStamp;
}FRAMNE_INFO;

typedef struct
{
	int year;
	int month;
	int day;
	int wday;
	int hour;
	int minute;
	int second;
}NetSDK_ALARM_TIME;

typedef struct
{
	NetSDK_ALARM_TIME time;
	int code;
	int flag;
	int level;
	char data[MAX_ALARM_DATA];
}NetSDK_ALARM_ITEM;

typedef struct
{
    int    lChannel;
    int    lLinkMode;
    int    hPlayWnd;
    char    *sMultiCastIP;
}*LPIP_NET_DVR_CLIENTINFO,IP_NET_DVR_CLIENTINFO;

#define			SERIALNO_LEN		48
typedef struct
{
    unsigned char     sSerialNumber[SERIALNO_LEN];
    unsigned char     byAlarmInPortNum;
    unsigned char     byAlarmOutPortNum;
    unsigned char     byDiskNum;
    unsigned char     byDVRType;
    unsigned char     byChanNum;
    unsigned char     byStartChan;
}*LPIP_NET_DVR_DEVICEINFO,IP_NET_DVR_DEVICEINFO;

typedef struct
{
    
}IP_NET_DVR_ALARMER;

#define MAX_IPADDR_LEN	64
typedef struct
{
	int  nVideoPort;
	int  bIsTcp;
	int  nVideoChannle;
	void *pUserData;
}USRE_VIDEOINFO,* LPUSRE_VIDEOINFO;



typedef struct  __STREAM_AV_PARAM
{
	unsigned char	ProtocolName[32];
	short  bHaveVideo;
	short  bHaveAudio;
	NetSDK_VIDEO_PARAM videoParam;
	NetSDK_AUDIO_PARAM audioParam;
    char  szUrlInfo[512];
}NetSDK_STREAM_AV_PARAM;




typedef struct __StateEventMsgInfo
{
	char szInfo[1024];
	char szUrlInfo[512];
}STATE_EVENT_MSGINFO;

typedef struct
{
	int		bIsKey;
	double	timestamp;
	void    *pUserData;
    int     nFlag;//==FRAME_V2_FLAG_VALUE 标识符，用于兼容旧版本标识
    int		nChannel;//流通道
}FRAME_EXTDATA,* LPFRAME_EXTDATA;
#define FRAME_V2_FLAG_VALUE	0x12340001

//2G
#define		AVI_RECORD_RESERVED_FREE_SPACE	2048


typedef struct _updpackhead
{
    unsigned int frame_timestamp;//unsigned long frame_timestamp;
    unsigned int keyframe_timestamp;//unsigned long keyframe_timestamp;
	unsigned short pack_seq;
	unsigned short payload_size;
	unsigned char pack_type;
	unsigned char frame_type;
	unsigned char stream_type;//0: video, 1: audio
	unsigned char stream_index;
	unsigned int  frame_index;
}NetSDK_UpdPackHead;

typedef struct
{
	int			stream_id;
	NetSDK_VIDEO_PARAM * video_param;
}NetSDK_VIDEO_STATE_MSG_PARAM;

typedef struct{
	unsigned int      dwSize;
	unsigned int       byDecChanScaleStatus;
}NET_DVR_MATRIX_DECCHAN_CONTROL;

typedef struct{
	char			sDVRIP[16];
	unsigned int	wDVRPort;
	unsigned int	wPTZPort;
	unsigned int    byChannel;
	unsigned int    byTransProtocol;
	unsigned int    byTransMode;
	char			sUserName[32];
	char			sPassword[32];
}NET_DVR_MATRIX_DECINFO;


typedef struct{
	unsigned int                    dwSize;
	NET_DVR_MATRIX_DECINFO   struDecChanInfo;
}NET_DVR_MATRIX_DYNAMIC_DEC;

#define MAX_VIDEO_CHAN  16
typedef struct{
	unsigned int                    dwEnable;
	unsigned int                    dwVideoNum;
	NET_DVR_MATRIX_DECINFO   struDecChanInfo[MAX_VIDEO_CHAN];
}NET_DVR_MATRIX_DECCHANINFO;

#define MAX_CYCLE_CHAN 16
typedef struct{
	unsigned int                        dwSize;
	unsigned int                        dwPoolTime;
	unsigned int                        dwPoolNum;
	NET_DVR_MATRIX_DECCHANINFO   struchanConInfo[MAX_CYCLE_CHAN];
}NET_DVR_MATRIX_LOOP_DECINFO;

typedef long(*SearchIPCCallBack)(long nEventCode,long index, NetSDK_IPC_ENTRY *pResponse,void *pUser);
typedef long(*MSGCallBack)(long lCommand,IP_NET_DVR_ALARMER *pAlarmer,char *pAlarmInfo,unsigned long BufLen,void *pUser);
typedef long(*StatusEventCallBack)(long lUser,long nStateCode,char *pResponse,void *pUser);
typedef long(*AUXResponseCallBack)(long lUser,long nType,char *pResponse,void *pUser);
typedef long(*fVoiceDataCallBack)(long lVoiceComHandle,char *pRecvDataBuffer,unsigned long dwBufSize,unsigned char byAudioFlag,LPFRAME_EXTDATA  pUser);
typedef long(*fRealDataCallBack)(long lRealHandle,unsigned long dwDataType,unsigned char *pBuffer,unsigned long dwBufSize,LPFRAME_EXTDATA  pExtData);
typedef long(*fPlayActionEventCallBack)(long lUser,long nType,long nFlag,char * pData,void * pUser);
typedef long(*fExceptionCallBack)(unsigned long dwType,long lUserID,long lHandle,void *pUser);
typedef	long(*fEncodeAudioCallBack)(long lType, long lPara1, long lPara2);
typedef long(*SerialDataCallBack)(long lUser, char *pRecvDataBuffer, unsigned long dwBufSize, void *pUser);
typedef long(*fRecFileNameCallBack)(long lRealHandle, char *pRecFileNameBuf, unsigned long dwBufSize, void *pUser);
typedef void(*fSDKLogCallBack)(const char* msg);




/*************************************************************************************************************************
*                                             设备直连访问相关接口(旧版本，目前已停用!)                                  *
*************************************************************************************************************************/
#if 1

void FC_SetSDKLogCallBack(fSDKLogCallBack pfcb);

int  FC_SetAutoReconnect(int nReconnect);



/*
 函数名称：FC_SetSearchDevStatusCallBack
 函数功能：设置搜索局域网设备响应回调函数
 参数说明：fSearchCallBack：搜索回调响应函数；pUser：用户数据,通过回调函数反回给用户;
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_SetSearchDevStatusCallBack(SearchIPCCallBack fSearchCallBack, void *pUser);

/*
 函数名称：FC_StartSearchDev
 函数功能：开始搜索局域网设备
 参数说明：无
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_StartSearchDev(void);

/*
 函数名称：FC_StopSearchDev
 函数功能：停止搜索局域网设备
 参数说明：无
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_StopSearchDev(void);

int FC_GetOneIPAddress(char * strResult, int nSize);

/*
 函数名称：FC_GetSearchIPCCount
 函数功能：获取当前已搜索到的设备数量
 参数说明：无
 返回值：0:函数调用成功；非0:返回成功搜索到是数量
 */
int FC_GetSearchIPCCount(void);

/*
 函数名称：FC_GetIPCInfo
 函数功能：获取IPC的信息
 参数说明：nIndex:要读取第几个设备（编号从0开始），小于FC_GetSearchIPCCount()函数返回值；pIPCInfo：信息保存指针
 返回值：0:函数调用成功；非0:失败
 */
int FC_GetIPCInfo(int nIndex, NetSDK_IPC_ENTRY *pIPCInfo);

/*
 函数名称：FC_RestoreIPC
 函数功能：将搜索到的设备直接恢复出厂
 参数说明：nIndex:要读取第几个设备（编号从0开始），小于FC_GetSearchIPCCount()函数返回值；pIPCInfo：信息保存指针
 返回值：0:函数调用成功；非0:失败
 */
int FC_RestoreIPC(int nIndex, NetSDK_IPC_ENTRY *pIPCInfo);

/*
 函数名称：FC_ModifyIPC
 函数功能：修改IPC
 参数说明：nIndex:要读取第几个设备（编号从0开始），小于FC_GetSearchIPCCount()函数返回值；pIPCInfo：信息保存指针
 返回值：0:函数调用成功；非0:失败
 注意：1、由于使用广播包进行修改，所以只能通过一直搜索，然后发现新的设备时通过序列号判断是否为我们修改的设备；
      2、当前只允许修改网络参数和服务端口
 */
int FC_ModifyIPC(int nIndex, NetSDK_IPC_ENTRY *pIPCInfo);


/*
 函数名称：FC_SetStatusEventCallBack
 函数功能：设置设备状态响应回调函数
 参数说明：fStatusCallBack：状态接收回调函数；pUser：用户数据，通过回调函数返回给用户
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_SetStatusEventCallBack(StatusEventCallBack fStatusCallBack, void *pUser);

/*
 函数名称：FC_SetAuxResponseCallBack
 函数功能：设置设备辅助通道响应回调函数
 参数说明：fAuxRespCallBack：回调函数；pUser：用户数据，通过回调函数返回给用户
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_SetAuxResponseCallBack(AUXResponseCallBack fAuxRespCallBack, void *pUser);

/*
 函数名称：FC_LoginDev
 函数功能：登陆直连设备
 参数说明：pDevIp：设备IP；nPort：设备辅助端口；pUser：设备登陆账号；pPassword：设备登陆密码
 返回值：<=0:函数调用失败；>0:函数调用成功，保存好返回值用于后续针对此设备的其他操作判断
 注意：设备是否登陆成功，通过状态回调函数进行通知
 */
long FC_LoginDev(char *pDevIp, int nPort, char *pUser, char *pPassword);

/*
 函数名称：FC_LoginDev
 函数功能：登陆直连设备
 参数说明：lUserId：调用FC_LoginDev设备登陆时的返回值
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_LogOutDev(long lUserId);

/*
 函数名称：FC_RealPlay
 函数功能：按登陆设备请求视频流
 参数说明：lUserId：调用FC_LoginDev设备登陆时的返回值;cbRealData:媒体流接收回调函数;pUser:用户数据
 返回值：0:函数调用失败；非0:函数调用成功，此路视频流的唯一标识，需要保存以便后续操作使用
 */
long FC_RealPlay(long lUserId, fRealDataCallBack cbRealData, LPUSRE_VIDEOINFO pUser);

/*
FunctionName:FC_RealPlayEx
Description:
Parameters:
Return: 0:failed, !=0:success
*/
long FC_RealPlayEx(long lUserId, char * serverip, char *user, char *pass, fRealDataCallBack cbRealDataCallBack, LPUSRE_VIDEOINFO pUser);

/*
 函数名称：FC_RealPlay
 函数功能：停止视频流
 参数说明：lRealHandle：调用FC_RealPlay时的返回值;
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_StopRealPlay(long lRealHandle);

/*
FunctionName: FC_StopAllRealPlay
Description: stop all realplay
Parameters:
Return: 0 success, !=0 failed
*/
int FC_StopAllRealPlay(void);



int FC_GetFileByName(long lUserId, long nFileType, char *sDVRFileName, char *saveDir);

int FC_StopGetFile(long lUserId);

int FC_MatrixStartDynamic(long lUserId, unsigned long dwDecChanNum, unsigned long dwVideoNum, NET_DVR_MATRIX_DYNAMIC_DEC lpDynamicInfo[MAX_VIDEO_CHAN]);

int FC_MatrixStopDynamic(long lUserId,  unsigned long dwDecChanNum);

int FC_MatrixSetLoopDecChanInfo(long lUserId, unsigned long dwDecChanNum, NET_DVR_MATRIX_LOOP_DECINFO lpInter);




/*
FunctionName: FC_SearchNVRRecByTime
Description: search NVR record by time
Parameters: lUserId @ FC_LoginDev return; lChannel@is NVR channel 0-31; pDate@search datetime "20150630"
Return: 0 success, !=0 failed
Mark: The record info recv from AUXResponseCallBack, command is 3001, using 1440 chars description, "ABCD......";
'A':TIMER RECORD
'B':ALARM RECORD
'C':MANUEL RECORD
*/
int FC_SearchNVRRecByTime(long lUserId, long lChannel, char *pDate);


/*
FunctionName: FC_NVRRecordDownload
Description: download NVR record
Parameters: lUserId@call FC_LoginDev return; lChannel@NVR channel 0-31; pStartTime@download record file start time;
                   pEndTime@download record file end time; pSaveFile@local save file
Return: 0 success, !=0 failed
*/
int FC_NVRRecordDownload(long lUserId, long lChannel, char *pStartTime, char *pEndTime, char *pSaveFile);

/*
 函数名称：FC_PTZControl
 函数功能：基本云台控制
 参数说明：lUserId：调用FC_LoginDev时的返回值;nPtzCmd：云台控制命令参考PTZ_CMD_TYPE枚举值；nHSpeed：云台水平速度；nVSpeed：云台垂直数度
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_PTZControl(long lUserId, int nPtzCmd, int nHSpeed, int nVSpeed);

/*
 函数名称：FC_PTZPreset
 函数功能：设置云台控制预置点
 参数说明：lUserId：调用FC_LoginDev时的返回值;nPtzCmd：命令参考PTZ_PRESET_TYPE枚举值；nPreset：要操作的预置点，可以是0-255.
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_PTZPreset(long lUserId, int nPtzCmd, int nPreset);

/*
 函数名称：FC_PTZControlEx
 函数功能：云台控制扩展接口，485透传
 参数说明：lUserId：调用FC_LoginDev时的返回值;pXml：云台操作xml格式，具体格式说明请参考文档
 返回值：0:函数调用成功；非0:函数调用失败
 */
int FC_PTZControlEx(long lUserId, char *pXml);

/*
 函数名称：FC_GetDevConfig
 函数功能：读取设备配置
 参数说明：lUserId：调用FC_LoginDev时的返回值; nCommand：配置对应信息，请参考文档
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回，如果成功则回调函数错误标记为0，消息类型（注意后面24位的值）与nCommand相同。
 */
int FC_GetDevConfig(long lUserId, unsigned long nCommand);

/*
 函数名称：FC_SetDevConfig
 函数功能：设置设备配置
 参数说明：lUserId：调用FC_LoginDev时的返回值; nCommand：配置对应信息，请参考文档; pXml:配置xml文本内容
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回，如果成功则回调函数错误标记为0，消息类型（注意后面24位的值）与nCommand相同。
 */
int FC_SetDevConfig(long lUserId, unsigned long nCommand, char *pXml);

/*
 函数名称：FC_SystemControl
 函数功能：对设备进行高级系统控制
 参数说明：lUserId：调用FC_LoginDev时的返回值; nCommand：配置对应信息，请参考文档; pXml:配置xml文本内容
 返回值：0:函数调用成功；具体的响应结果都通过辅助通道回调函数返回，如果成功则回调函数错误标记为0，消息类型（注意后面24位的值）与nCommand相同。
 */
int FC_SystemControl(long lUserId, unsigned long nCommand, char *pXml);

#endif
int FC_MyDecrypt(char *pkey, char *pDat);

//search and modify IPC
int FC_Loc_SetSearchStatusCallBack(SearchIPCCallBack fcallBack,void * pUser);
int FC_Loc_StartSearchIPC(void);
int FC_Loc_StopSearchIPC(void);
int FC_Loc_GetSearchIPCCount(void);
int FC_Loc_GetIPCInfo(long index, NetSDK_IPC_ENTRYV2 * pIPCInfo);
int FC_Loc_ModifyIPC(long index, NetSDK_IPC_ENTRYV2 * pIPCInfo);
int FC_Loc_GetIPCInfoXML(long index, char * pXMLInfo,int maxLen);
int FC_Loc_ModifyIPCXML(long index, const char * strXML);
int FC_Loc_GetOneIPAddress(char * strResult,int nSize);
int FC_Loc_GetNetworkParam(long nParamIndex, char * strResult,int nSize);
int FC_Loc_RestoreIPC(long index, NetSDK_IPC_ENTRYV2 * pIPCInfo);
//login IPC
int FC_Loc_SetStatusEventCallBack(StatusEventCallBack fStatusEventCallBack,void * pUser);
long FC_Loc_LoginDev(char *sDVRIP,unsigned long wDVRPort,char *sUserName,char *sPassword,LPIP_NET_DVR_DEVICEINFO lpDeviceInfo);
int FC_Loc_LogoutDev(long lDevItem);
//PTZ
int FC_Loc_PTZControlEx(long lDevItem,char *pXml);
int FC_Loc_PTZPreset(long lDevItem,unsigned long dwPTZPresetCmd,unsigned long dwPresetIndex);
int FC_Loc_PTZControl(long lDevItem,unsigned long dwPTZCommand,unsigned long nTspeed,unsigned long nSpeed);
int FC_Loc_SetDVRMessageCallBack(MSGCallBack fMessageCallBack,void *pUser);
//config
int FC_Loc_GetDVRConfig(long lDevItem,unsigned long dwCommand,long lChannel,void* lpOutBuffer,unsigned long dwOutBufferSize,unsigned long* lpBytesReturned);
int FC_Loc_SetDVRConfig(long lDevItem,unsigned long dwCommand,long lChannel,void* pXml,unsigned long dwInBufferSize);
int FC_Loc_SystemControl(long lDevItem,unsigned long nCmdValue,long flag,char * pXml);
int FC_Loc_WriteAUXStringEx(long lDevItem,char * pMsgType,long nCode,long flag,char * pXml);
int FC_Loc_GetUserData(long lDevItem,char * pOutBuffer,int* nInOutLen);
int FC_Loc_SetUserData(long lDevItem,char * pBuffer,int len);
int FC_Loc_CreateIFrame(long lDevItem,int bIsSubStream);
int FC_Loc_RestoreConfig(long lDevItem);
int FC_Loc_RebootDVR(long lDevItem);
int FC_Loc_ShutDownDVR(long lDevItem);
int FC_Loc_GetDeviceAbility(long lDevItem);
int FC_Loc_FormatDisk(long lDevItem, long lDiskNumber);
int FC_Loc_Upgrade(long lDevItem, char *sFileName);
int FC_Loc_GetUpgradeProgress(long lDevItem);
int FC_Loc_GetUpgradeState(long lDevItem);
int FC_Loc_CloseUpgradeHandle(long lDevItem);
int FC_Loc_GetFileByName(long lDevItem,long nFileType,char *sDVRFileName,char *saveDir);
int FC_Loc_StopGetFile(long lDevItem);
int FC_Loc_GetDownloadState(long lDevItem);
int FC_Loc_GetDownloadPos(long lDevItem);
int FC_Loc_SerialStart(long lDevItem, SerialDataCallBack cbSDCallBack, void* pUser);
int FC_Loc_SerialSend(long lDevItem, long lChannel, char *pSendBuf);
int FC_Loc_SerialStop(long lDevItem);
int FC_Loc_PostSerialMsg(void* obj, void* pAlarm);
int FC_Loc_MatrixStartDynamic(long lDevItem, unsigned long dwDecChanNum, unsigned long dwVideoNum, NET_DVR_MATRIX_DYNAMIC_DEC lpDynamicInfo[MAX_VIDEO_CHAN]);
int FC_Loc_MatrixStopDynamic(long lDevItem,  unsigned long dwDecChanNum);
int FC_Loc_MatrixSetLoopDecChanInfo(long lDevItem, unsigned long dwDecChanNum, NET_DVR_MATRIX_LOOP_DECINFO lpInter);
int FC_Loc_GetConfigFile(long lDevItem,char *sFileName);
int FC_Loc_SetConfigFile(long lDevItem,char *sFileName);
int FC_Loc_FindDVRLogFile(long lDevItem);
//stream
long FC_Loc_RealPlay(long lDevItem,fRealDataCallBack cbRealDataCallBack,LPUSRE_VIDEOINFO pUser);
long FC_Loc_RealPlayEx(long lDevItem,char * serverip,char *user,char *pass,fRealDataCallBack cbRealDataCallBack,LPUSRE_VIDEOINFO pUser);
int FC_Loc_StopRealPlay(long lRealHandle);
int FC_Loc_StopAllRealPlay(void);
int FC_Loc_GetVideoParam(long  lRealHandle, NetSDK_VIDEO_PARAM *pParam);
int FC_Loc_GetAudioParam(long lRealHandle, NetSDK_AUDIO_PARAM *pParam);
int FC_Loc_SetRealDataCallBack(fRealDataCallBack cbRealDataCallBack,void * dwUser);
//record



/*
 函数名称：
        FC_Loc_SetRecFileNameCallBack
 函数功能：
        如果需要自定制录制文件名，则使用此函数设置回调。
        设置后，创建文件之前则会进行回调，让用户进行自定义文件名。
 参数说明：
        lRealHandle:要录制的标识句柄，可以是FC_Loc_StartRecordStream返回的句柄
                    也可以是FC_Loc_RealPlayEx，FC_Loc_RealPlay调用时的返回句柄
        cbRecFileNameCallBack:[in]调用FC_Loc_StartRecordStream时的标识句柄
        pUser：[in]回调函数返回给用户的指针
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_SetRecFileNameCallBack(long lRealHandle
                                  , fRecFileNameCallBack cbRecFileNameCallBack
                                  , void *pUser);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StartRecord(long lRealHandle,const char * filePath,int nFileMaxSeconds,int nAllRecordMaxSeconds);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StopRecord(long lRealHandle);


/*
 函数名称：FC_Loc_StartRecordStreamEx
 函数功能：创建一个写MP4的文件，用于写数据
 参数说明：
            pAvParam:[in]编码参数
            filePath:[in]存储文件名
     nFileMaxSeconds:[in]输入参数 音频格式，支持AAC和G711
nAllRecordMaxSeconds:[in]最大录制时长
             bIsSub:[in]是否为子码流
 返回值：0:失败，非0:成功，返回值表示文件句柄,本头文件中，使用lRecrodHandle标识
 */
long FC_Loc_StartRecordStreamEx(NetSDK_STREAM_AV_PARAM * pAvParam
                                ,const char * filePath
                                ,int nFileMaxSeconds
                                ,int nAllRecordMaxSeconds
                                ,int bIsSub
                                ,fRecFileNameCallBack cbRecFileNameCallBack
                                ,void *pUser);
/*
 函数名称：FC_Loc_OpenWrite
 函数功能：创建一个写MP4的文件，用于写数据
 参数说明：
            pAvParam:[in]编码参数
            filePath:[in]存储文件名
     nFileMaxSeconds:[in]输入参数 音频格式，支持AAC和G711
nAllRecordMaxSeconds:[in]最大录制时长
 返回值：0:失败，非0:成功，返回值表示文件句柄,本头文件中，使用lRecrodHandle标识
 */
long FC_Loc_StartRecordStream(NetSDK_STREAM_AV_PARAM * pAvParam,const char * filePath
                              ,int nFileMaxSeconds,int nAllRecordMaxSeconds);


/*
 函数名称：FC_Loc_InputRecordStream
 函数功能：当采用FC_Loc_StartRecordStream模式进行录像时，传入要录制的视频数据
 参数说明：
         pFile:[in]写入的文件名
        pVideo:[in]输入参数 视频参数,仅支持H264和H265,不能为空
        pAudio:[in]输入参数 音频格式，支持AAC和G711
 返回值：0:成功，非0:失败
 */
int FC_Loc_InputRecordStream(long lRecrodHandle,const void * pBuffer,int nSize,int isVideo,int iskey,double timestamp);


/*
 函数名称：FC_Loc_StopRecordStream
 函数功能：当采用FC_Loc_StartRecordStream模式进行录像时，停止录像
 参数说明：
 lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：0:成功，非0:失败
 */
int FC_Loc_StopRecordStream(long lRecrodHandle);


//replay
/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_GetReplayAblity(long lDevItem);




/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_PlayDeviceFile(long lDevItem,char * filenme);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_SetReplayDataCallBack(fRealDataCallBack cbReplayDataCallBack
                                 ,void *dwUser);






/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_SetPlayActionEventCallBack(fPlayActionEventCallBack cbActionCallback
                                      ,void *dwUser);




/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_ControlPlay(long lDevItem,long Action,long param);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_SearchNVRRecByTime(long lDevItem, long lChannel, char *pDate);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_NVRReplayByTime(long lDevItem, long lChannel, char *pDateTime);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_ControlNVRReplay(long lDevItem, long lChannel, long lAction
                            , long lSpeed, char *pPlayTime);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_SetAUXResponseCallBack(AUXResponseCallBack fAUXCallBack,void * pUser);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_NVRRecordDownload(long lDevItem, long lChannel, char *pStartTime
                             , char *pEndTime, char *pSaveFile);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_NVRRecordDownloadStop(long lDevItem, long lChannel);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
//speak
int FC_Loc_StartSpeak(long lDevItem);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StopSpeak(long lDevItem);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_InputSpeakAudioData(long lDevItem, TPS_AudioData oAudioData);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_SetVoiceComClientVolume(long lVoiceComHandle,unsigned long wVolume);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StopVoiceCom(long lVoiceComHandle);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StartVoiceCom(long lDevItem,unsigned long dwVoiceChan
                         ,bool bNeedCBNoEncData
                         ,fVoiceDataCallBack cbVoiceDataCallBack,void *pUser);




/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StartTalk(int audiotype, int samplerate, int bitspersample, int channels);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_StopTalk(void);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_AddTalk(long lDevItem);



/*
 函数名称：

 函数功能：

 参数说明：
        lRecrodHandle:[in]调用FC_Loc_StartRecordStream时的标识句柄
 返回值：
        0:成功，非0:失败
 */
int FC_Loc_RemoveTalk(long lDevItem);



/*
 函数名称：FC_Loc_OpenWrite
 函数功能：创建一个写MP4的文件，用于写数据
 参数说明：
         pFile:[in]写入的文件名
        pVideo:[in]输入参数 视频参数,仅支持H264和H265,不能为空
        pAudio:[in]输入参数 音频格式，支持AAC和G711
 返回值：0:失败，非0:成功，返回值表示文件句柄
 */
MP4FILE_HANDLE	FC_Loc_OpenWrite(const char * pFile,NetSDK_VIDEO_PARAM * pVideo,NetSDK_AUDIO_PARAM * pAudio);


/*
 函数名称：FC_Loc_OpenRead
 函数功能：创建一个读取MP4的文件，用于读取数据
 参数说明：
         pFile:[in]读取的文件名
        pVideo:[out]输出参数 视频参数,仅支持H264和H265,不能为空
        pAudio:[out]输出参数 音频格式，支持AAC和G711,不能为空
 返回值：0:失败，非0:成功，返回值表示文件句柄
 */
MP4FILE_HANDLE	FC_Loc_OpenRead(const char * pFile,NetSDK_VIDEO_PARAM * pVideo,NetSDK_AUDIO_PARAM * pAudio);



/*
 函数名称：FC_Loc_GetMp4ReadWriteLastError
 函数功能：当调用FC_Loc_OpenWrite FC_Loc_OpenRead后，返回空时，调用此函数返回错误代码
 参数说明：
 返回值：0:无错误
        非0:成功，返回值表示文件句柄
 */
int     FC_Loc_GetMp4ReadWriteLastError(void);

/*
 函数名称:FC_Loc_CloseFile
 函数功能:关闭一个文件
 参数说明:
    fHandle:文件名柄
 返回值：0:成功，非0:失败
 */
int		FC_Loc_CloseFile(MP4FILE_HANDLE fHandle);


/*
 函数名称:FC_Loc_WriteOneFrame
 函数功能:写入一帧数据
 参数说明:
    fHandle:[in]文件名柄，必须是以FC_Loc_OpenWrite返回的文件句柄
    isvideo:[in]是否视频数据，1表示视频，0表示音频
    pBuffer:[in]媒体数据,数据必须是带00 00 00 01的数据头
    nInSize:[in]媒体数据长度
  timestamp:[in]当前视频的时截
      iskey:[in]是否关键帧，如果是音频，则都填1
 返回值：0:成功
     ERR_NOT_FIND_FILEHANDLE:文件句柄错误
     ERR_OUTOFF_MEMORY:内存不足
     其它非0:失败
 */
int		FC_Loc_WriteOneFrame(MP4FILE_HANDLE fHandle,int isvideo
                             ,const void * pBuffer,int nInSize
                             ,long long  timestamp,int iskey);


/*
 函数名称:FC_Loc_ReadOneFrame
 函数功能:读取一帧数据
 参数说明:
    fHandle:[in]文件名柄，必须是以FC_Loc_OpenRead返回的文件句柄
    isvideo:[out]是否视频数据，1表示视频，0表示音频
    pBuffer:[out]媒体数据指针
nMaxOutSize:[in out]媒体数据指针可存储的最大长度
  timestamp:[out]当前视频的时截
   duration:[out]当前帧时长
      iskey:[out]是否关键帧，如果是音频，则都填1
 返回值：0:成功
     ERR_NOT_FIND_FILEHANDLE:文件句柄错误
     ERR_OUTOFF_MEMORY:内存不足
     其它非0:失败
 */
int		FC_Loc_ReadOneFrame(MP4FILE_HANDLE fHandle
                            ,int *isvideo
                            ,void *pBuffer
                            ,int  *nMaxOutSize
                            ,long long   *timestamp
                            , long long  *duration
                            , int  *iskey);


/*
 函数名称:FC_Loc_GetAllTime

 函数功能:获取读取的文件总时长，返回值以秒为单位

 参数说明:
    fHandle:[in]文件名柄，必须是以FC_Loc_OpenRead返回的文件句柄

 返回值：
                  大于0:表示返回文件长度
ERR_NOT_FIND_FILEHANDLE:句柄错误
      ERR_NOT_READMODE:文件句柄不是读取模式
                其它负值:失败
 */
int		FC_Loc_GetAllTime(MP4FILE_HANDLE fHandle);


/*
 函数名称:FC_Loc_GetNowTime
 函数功能:读取当前读取文件的位置，返回值以秒为单位
 参数说明:
    fHandle:[in]文件名柄，必须是以FC_Loc_OpenRead返回的文件句柄
 返回值：大于0，表示返回当前读取位置
       ERR_NOT_FIND_FILEHANDLE：句柄错误
        ERR_NOT_READMODE:文件句柄不是读取模式
       其它负值:失败
 */
int		FC_Loc_GetNowTime(MP4FILE_HANDLE fHandle);


/*
 函数名称:FC_Loc_SeekToFile
 函数功能:跳到指定位置进行读取
 参数说明:
     fHandle:[in]文件名柄，必须是以FC_Loc_OpenRead返回的文件句柄
    toSecond:[in]读取位置，单位为秒
 返回值：0:成功
       ERR_NOT_FIND_FILEHANDLE：句柄错误
        ERR_NOT_READMODE:文件句柄不是读取模式
       其它非0:失败
 */
int		FC_Loc_SeekToFile(MP4FILE_HANDLE fHandle,int toSecond);


/*
 函数名称:FC_Loc_MoveNextKeyFrame
 函数功能:跳至下一个视频关键帧
 参数说明:
     fHandle:[in]文件名柄，必须是以FC_Loc_OpenRead返回的文件句柄
 返回值：0:成功
       ERR_NOT_FIND_FILEHANDLE：句柄错误
        ERR_NOT_READMODE:文件句柄不是读取模式
       其它非0:失败
 */
int		FC_Loc_MoveNextKeyFrame(MP4FILE_HANDLE fHandle);


/*
 函数名称:FC_Loc_MovePrevKeyFrame
 函数功能:跳至上一个视频关键帧
 参数说明:
     fHandle:[in]文件名柄，必须是以FC_Loc_OpenRead返回的文件句柄
 返回值：0:成功
       ERR_NOT_FIND_FILEHANDLE：句柄错误
        ERR_NOT_READMODE:文件句柄不是读取模式
       其它非0:失败
 */
int		FC_Loc_MovePrevKeyFrame(MP4FILE_HANDLE fHandle);




/*
 函数名称:FC_Loc_GetRecordTimeFromFile
 函数功能:读取录像文件的总时长
 参数说明:
     pFileName:[in]文件名称，打开后会自动关闭，用于仅获取文件录像时长时使用
 返回值：大于0，表示录像文件的时长
        ERR_PARAM_ERROR:文件名错误
        ERR_OUTOFF_MEMORY:申请内存失败，系统内存不足
        ERR_OPEN_FILEERROR:打开文件失败
        ERR_MP4FILE_FORMAT_ERROR:读取文件失败，可能是文件格式错误
        其它非0:失败
 */
int		FC_Loc_GetRecordTimeFromFile(const char * pFileName);





#endif




