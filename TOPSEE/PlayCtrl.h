#define SDK_IOS 1 
//
//  PlayCtrl.h
//  PlayCtrl
//
//  Created by 廖海洲 on 14-2-28.
//  Copyright (c) 2014年 Topsee. All rights reserved.
//

#ifndef __PLAY_CTROL_H__
#define __PLAY_CTROL_H__

#ifdef SDK_IOS
//for display
#import <UIKit/UIKit.h>
#else
	class UIView
	{
	public:
	    UIView();
	    ~UIView();
	};
#endif

typedef struct
{
    int  nWidth;
    int  nHeight;
    int  nTimestamp;
    int  nType;
    int  nFrameRate;
    int  nIsVideo;
    int   nLineSize[4];
}FRAME_INFO;

enum  PLAYER_ERR_CODE
{
    ERR_NO_INIT_AACENCODER,
    ERR_OUT_OFF_MEMORY,
    ERR_INPUT_PARAM_ERROR,
    ERR_INPUT_FRAME_SIZE_TOOLARGE,
};


typedef struct __RecvVideo_Info
{
    int     nRecvVideoFrame;
    int     nOutputVideoPicCount;
    int     nAvgRecvCount;
    int     nAvgOutputCount;
}RecvVideo_Info;


enum
{
    TPS_PLAY_EVENT_SNAPSHOT_OK = 1,
    TPS_PLAY_EVENT_SNAPSHOT_FAILED,
    EVENT_PLAYER_AUDIO_DEV_THREAD_START,
    EVENT_PLAYER_AUDIO_DEV_THREAD_EXIT,
    EVENT_PLAYER_VIDEO_DEV_THREAD_START,
    EVENT_PLAYER_VIDEO_DEV_THREAD_EXIT,
    EVENT_PLAYER_VIDEO_FRAME_AVG_INFO,//  report recv and output pic count, pEventInfo=RecvVideo_Info *
    EVENT_PLAYER_VIDEO_FRAME_LOSS_INFO,// report video lost frame and skip frame count. pEventInfo=int *
};


typedef void (*fcLogCallBack)(unsigned int uLevel, const char* lpszOut);
typedef int (*fDecDataCallBack)(int nPort, char *pDecData, int nSize, FRAME_INFO *pFrameInfo, void *pUser);
typedef int (*fEventCallBack)(int nPort, int nEventType, char *pEventInfo, int nInfoDataLen, void *pUser);
typedef int (*fThreadStatusCallBack)(int nPort, int nMediaType, int nStatus, void *pUser);


int PC_Init(void);

int PC_Free(void);

int PC_GetProt(void);

int PC_FreeProt(int nPort);


int PC_SetfcLogCallBack(fcLogCallBack fMsgRspCallBack);
int PC_SetfcLogCallBackEx(fcLogCallBack fMsgRspCallBack,int nLevel);

int PC_SetDecCallBack(int nPort, fDecDataCallBack fDecFunc, void *pUser);

int PC_SetEventCallBack(int nPort, fEventCallBack fEventFunc, void *pUser);
int PC_SetThreadStatusCallBack(fThreadStatusCallBack fThreadStatusFunc, void *pUser);
int PC_DisableTrimYuvData(int disable);


int PC_OpenStream(int nPort, char* pParam, int nSize, int isAudio, int nMaxBufFrameCount);

int PC_InputVideoData(int nPort,  char* pBuf, int nSize, int isKey, unsigned int timestamp);

int PC_InputAudioData(int nPort,  char* pBuf, int nSize, unsigned int timestamp);

#ifdef SDK_IOS
int PC_Play(int nPort, UIView *pUIView);//pUIView非空直接播放, pUIView为空时只解码不播放，解码后的YUV数据通过回调函数返回
#else
int PC_Play(int nPort, int nRgbBits);//nRgbBits=0表示yuv数据, 非0表示对应bits的rgb数据
//nRgbBits=0表示yuv数据, 非0表示对应bits的rgb数据, type: 0=v, 1=a, 2=av
int PC_PlayEx(int nPort, int nRgbBits, int type);
#endif

int PC_SnapShot(int nPort, char *pSavePath);
int PC_SnapShotEx(int nPort, char *pSavePath, int nType);

/**
 * @brief PC_Stop
 * 停止播放，注意，停止后播放器还在，需要调用PC_FreeProt释放
 * @param nPort
 * @return
 */
int PC_Stop(int nPort);


/**
 * @brief PC_FreeAll
 * 释放所有播放器
 * @return
 */
int PC_FreeAll(void);

int PC_YUV2RGB(int nWidth, int nHeight, char *pYuvBuffer, char *pRgbBuffer, int nBits);//support RGB32\RGB24\RGB16

int PC_G711Encode(short *dst, short *src, short bufsize);//g711 音频编码

int PC_PlaySound(int nPort);

int PC_StopSound(void);

int PC_EnableNoiseReduction(int nPort, int enable);

/**
 * @brief PC_SetPlayMode 设置是否回放模式，本函数必须调用OpenStream之后调用
 * @param nPort 播放端口
 * @param bIsReplayMode 1表示回放模式
 * @return
 */
int PC_SetPlayMode(int nPort,int bIsReplayMode);

/**
 * @brief PC_InitAACEncode 初始化AAC音频编码器
 * @param nSampleRate 采集率 一般为16000
 * @param nSampleWidth 采集精度 16 (bits)
 * @param nChannel 音频声道数(2)
 * @return 返回0表示成功，否则表示错误代码
 */
int PC_InitAACEncode(int nSampleRate,int nSampleWidth,int nChannel);

/**
 * @brief PC_AACEncode 压缩AAC音频
 * @param dst 压缩后，音频存放的地方，注意此内存必须足够大，建议不小于src大小
 * @param src 采集的原始音频数据*
 * @param bufsize 音频原始数据的字节数,只能为4096
 * @return 返回大于0表示实际输出字节数，小于0表示失败,-10088表示长度不正确
 */
int PC_AACEncode(char *dst, const char *src, int  nSize);

/**
 * @brief PC_DeleteAACEncode 释放AAC编码器
 * @return 返回0表示成功
 */
int PC_DeleteAACEncode(void);



/**
 * 初始化显示模块，显示之前需要先调用此函数
 * @param x 显示的开始X位置，一般为0
 * @param y 显示的开始的Y位置，一般为0
 * @param width 显示的宽度
 * @param height 显示的高度
 * @return 0表示成功，否则表示错误代码
*/
int PC_InitFishEyeMode(int nPort, char* pDeviceID,int x,int y,int width, int height, int nMode);


/**
 * 更改显示模式
 * @param mode 模式类型
 * 0表示原始图
 * 1表示去边图（调试）
 * 4显示4界面（360度时）
 * 180表示180度
 * 360表示360度
 * 其它值等同于180
 * @return 返回实际生效的模式值
 */
int  PC_ChangeFishEyeMode(int nPort,int mode);

/**
 * 移动和缩放控制
 * @param x 缩放比例,值应为30至100，表示视角
 * @param y 左右控制
 * @param z 上下控制(或旋转)
 * @return 0表示成功，否则表示错误代码
 */
int  PC_DoFishEyeMoveView(int nPort,float x,float y,float z);


/**
 * 初始化显示模块，显示之前需要先调用此函数
 * @param x 显示的开始X位置，一般为0
 * @param y 显示的开始的Y位置，一般为0
 * @param width 显示的宽度
 * @param height 显示的高度
 * @return 0表示成功，否则表示错误代码
*/
int PC_DisplayFishEyeVideoFrame(int nPort);



#endif
